import appRootPath from 'app-root-path'
import path from 'path'
import express from 'express'
import cors from 'cors'
import bodyParser from 'body-parser'
import boolParser from 'express-query-boolean'
import morgan from 'morgan'
import YAML from 'yamljs'
import swaggerUi from 'swagger-ui-express'
import M from './middlewares'

const app = express()
const swaggerDocument = YAML.load(`${appRootPath}/docs/api/swagger.yaml`);
app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument))
// require(path.join(appRootPath.toString(), 'src', 'middlewares', 'swaggerStatistics.js'))(app)

const whitelist = [process.env.FRONTEND_SERVER_URL, process.env.BACKEND_SERVER_URL]
const corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1 || !origin) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  }
}

app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(boolParser())
app.use(morgan('dev'))

app.get('/', (req, res) => {
  res.send({ msg: `welcome to '${process.env.NODE_ENV}' ${process.env.PROJECT_NAME}!` })
})

// global middleware
// app.use(M.validateToken)

// router config
// TODO: register router automatically
require('./routes/employee.route')(app)
require('./routes/sms.route')(app)
require('./routes/store.route')(app)
require('./routes/openingHour.route')(app)
require('./routes/calendar.route')(app)
require('./routes/photo.route')(app)
require('./routes/serviceProvider.route')(app)
require('./routes/mail.route')(app)
require('./routes/service.route')(app)
require('./routes/site.route')(app)

// 404 handling
app.use(M.handleNotFound)

// error handling
app.use(M.handleError)

module.exports = app
