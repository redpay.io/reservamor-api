const logger = require('../libs/winston')
const { ApiError } = require('../libs/Errors')
const R = require('ramda')

export default function requireIdentity(requiredIdentityList) {
  // 代表list裡的identity有一個符合即可
  return (req, res, next) => {
    try {
      logger.info('middleware.requiredIdentity() invoke')

      if (!req.isAuth) {
        throw new ApiError('Invalid Authorization', 401)
      }

      const requireEmployee = (R.contains('EMPLOYEE', requiredIdentityList) && req.currentEmployee)
      const requireUser = (R.contains('USER', requiredIdentityList) && req.currentUser)
      if (requireEmployee || requireUser) {
        next()
      } else {
        throw new ApiError('Invalid Authorization', 401)
      }
    } catch (err) {
      next(err)
    }
  }
}
