const logger = require('../libs/winston')
const { ApiError } = require('../libs/Errors')
const { Employee, User, sequelize } = require('../models')

export default async function attachProfile(req, res, next) {
  try {
    logger.info('middleware.attachProfile() invoke')
    if (req.currentEmployeeId) {
      const employee = await Employee.findByPk(req.currentEmployeeId)
      if (employee && employee.enable) {
        req.currentEmployee = employee
        req.currentEmployeeRole = (await employee.getRole()).name
        req.isAuth = true
      }
    } else if (req.currentUserId) {
      console.log('req.currentUserId', req.currentUserId)
      const user = await User.findByPk(req.currentUserId)
      if (user && user.enable) {
        req.currentUser = user
        req.isAuth = true
      }
    }
    next()
  } catch (err) {
    next(err)
  }
}
