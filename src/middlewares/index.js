const fs = require('fs');
const path = require('path');
const basename = path.basename(__filename);
const middleware = {}
fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    middleware[file.substring(0, file.lastIndexOf('.'))] = require(path.join(__dirname, file)).default;
  });

export default middleware
