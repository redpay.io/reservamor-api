const logger = require('../libs/winston')
const { ApiError } = require('../libs/Errors')

export default function handleError(err, req, res, next) {
  logger.error(err.stack)
  if (err instanceof ApiError) {
    res.status(err.statusCode).send({
      statusCode: err.statusCode,
      errorCode: null,
      message: err.toString()
    })
  } else if (err instanceof Error) {
    res.status(500).send({
      statusCode: 500,
      errorCode: null,
      message: err.toString()
    })
  } else {
    res.status(500).send({ message: 'something went wrong!' })
  }
}
