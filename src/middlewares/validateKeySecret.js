const logger = require('../libs/winston')
const { ApiError } = require('../libs/Errors')
const crypto = require('crypto')

export default function validateKeySecret(req, res, next) {
  try {
    logger.info('middleware.validateKeySecret() invoke')
    const timestamp = req.headers['x-timestamp']
    const key = req.headers['x-key']
    const secret = req.headers['x-secret']

    if (!timestamp) {
      throw new ApiError('Missing x-timestamp Header', 401)
    } else if (!key) {
      throw new ApiError('Missing x-key Header', 401)
    } else if (!secret) {
      throw new ApiError('Missing x-secret Header', 401)
    }

    let hash = crypto.createHash('md5').update(timestamp + key).digest("hex").toLowerCase()
    // TODO: check timestamp expired

    if (secret !== hash) {
      throw new ApiError('Invalid x-secret', 401)
    }
    next()
  } catch (err) {
    next(err)
  }
}
