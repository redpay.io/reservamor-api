const logger = require('../libs/winston')
const jwt = require('../libs/jwt')
const { ApiError } = require('../libs/Errors')

export default function validateToken(req, res, next) {
  try {
    logger.info('middleware.validateToken() invoke')
    const backendToken = req.headers['x-backend-auth']
    const frontendToken = req.headers['x-frontend-auth']

    if (backendToken && jwt.verify(backendToken)) {
      const payload = jwt.decode(backendToken).payload
      req.currentEmployeeId = payload.id
    } else if (backendToken && !jwt.verify(backendToken)) {
      throw new ApiError('Invalid Authorization', 401)
    } else if (frontendToken && jwt.verify(frontendToken)) {
      const payload = jwt.decode(frontendToken).payload
      req.currentUserId = payload.id
    } else if (frontendToken && !jwt.verify(frontendToken)) {
      throw new ApiError('Invalid Authorization', 401)
    }
    next()
  } catch (err) {
    next(err)
  }
}
