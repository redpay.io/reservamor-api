const logger = require('../libs/winston')
const { ApiError } = require('../libs/Errors')
const employeeService = require('../services/employee.service')

export default async function validateBackendPassword(req, res, next) {
  try {
    logger.info('middleware.validateBackendPassword() invoke')

    const authorization = req.headers.authorization
    if (!authorization || authorization.indexOf('Basic ') < 0) {
      throw new ApiError('Missing Authorization Header', 401)
    }

    // verify auth credentials
    const base64Credentials = authorization.split(' ')[1]
    const [username, password] = Buffer.from(base64Credentials, 'base64').toString('ascii').split(':')
    const employee = await employeeService.authenticate({ username, password })
    if (!employee) {
      throw new ApiError('Invalid Authentication Credentials', 401)
    }

    req.currentEmployeeId = employee.id
    next()
  } catch (err) {
    next(err)
  }
}
