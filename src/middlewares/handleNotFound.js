export default function (req, res, next) {
  res.status(404).send({
    statusCode: 404,
    message: 'NOT FOUND'
  })
}
