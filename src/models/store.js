const orderid = require('order-id')(process.env.STORE_NO_SECRET)

'use strict';
module.exports = (sequelize, DataTypes) => {
  const Store = sequelize.define('Store', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    no: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    phoneNumber: {
      type: DataTypes.STRING,
      allowNull: false
    },
    city1: {
      type: DataTypes.STRING,
      allowNull: false
    },
    city2: {
      type: DataTypes.STRING,
      allowNull: false
    },
    address: {
      type: DataTypes.STRING,
      allowNull: false
    },
    status: {
      type: DataTypes.ENUM,
      allowNull: false,
      values: ['DRAFT', 'READY', 'ACTIVE'],
      defaultValue: 'DRAFT'
    },
    logoFileId: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: 'File',
        key: 'id'
      }
    },
    communityUrl: {
      type: DataTypes.STRING,
      allowNull: true
    },
    isBranch: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    latitude: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    longitude: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    placeId: {
      type: DataTypes.STRING,
      allowNull: true
    },
    rating: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    employeeStoreGroupId: {
      type: DataTypes.UUID,
      allowNull: true,
      references: {
        model: 'EmployeeStoreGroup',
        key: 'id'
      }
    },
    description: {
      type: DataTypes.STRING(1024),
      allowNull: true
    },
  }, {});
  Store.associate = function (models) {
    Store.belongsTo(models.EmployeeStoreGroup, {
      foreignKey: 'employeeStoreGroupId',
      as: 'group'
    })
    Store.belongsTo(models.File, {
      foreignKey: 'logoFileId',
      as: 'logo'
    })
    Store.hasMany(models.OpeningHour, {
      foreignKey: 'storeId',
      as: 'openingHours'
    })
    Store.hasMany(models.Calendar, {
      foreignKey: 'storeId',
      as: 'calendars'
    })
    Store.hasMany(models.Service, {
      foreignKey: 'storeId',
      as: 'services'
    })
    Store.genNo = function () {
      const id = orderid.generate()
      return 'ST' + id.replace(/-/g, '')
    }
  };
  return Store;
};
