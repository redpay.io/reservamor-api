'use strict';
module.exports = (sequelize, DataTypes) => {
  const Calendar = sequelize.define('Calendar', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    storeId: {
      type: DataTypes.UUID,
      allowNull: true,
      references: {
        model: 'Store',
        key: 'id'
      }
    },
    year: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    month: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    closeDate: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    serviceProviderId: {
      type: DataTypes.UUID,
      allowNull: true,
      references: {
        model: 'ServiceProvider',
        key: 'id'
      }
    },
  }, {});
  Calendar.associate = function (models) {
    // associations can be defined here
  };
  return Calendar;
};
