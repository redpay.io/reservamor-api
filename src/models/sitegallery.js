'use strict';
module.exports = (sequelize, DataTypes) => {
  const SiteGallery = sequelize.define('SiteGallery', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    siteId: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: 'Site',
        key: 'id'
      }
    },
    fileId: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: 'File',
        key: 'id'
      }
    },
  }, {});
  SiteGallery.associate = function (models) {
    // associations can be defined here
  };
  return SiteGallery;
};
