'use strict';
module.exports = (sequelize, DataTypes) => {
  const OpeningHour = sequelize.define('OpeningHour', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    storeId: {
      type: DataTypes.UUID,
      allowNull: true,
      references: {
        model: 'Store',
        key: 'id'
      }
    },
    weekDay: {
      type: DataTypes.ENUM,
      allowNull: false,
      values: ['0', '1', '2', '3', '4', '5', '6']
    },
    isBusinessDay: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    periodName: {
      type: DataTypes.STRING,
      allowNull: true
    },
    openTime: {
      type: DataTypes.STRING,
      allowNull: true
    },
    closeTime: {
      type: DataTypes.STRING,
      allowNull: true
    },
  }, {});
  OpeningHour.associate = function (models) {
    // associations can be defined here
  };
  return OpeningHour;
};
