'use strict';
module.exports = (sequelize, DataTypes) => {
  const File = sequelize.define('File', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    originName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    sysName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    mimeType: {
      type: DataTypes.STRING,
      allowNull: true
    },
    size: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    imageWidth: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    imageHeight: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
  }, {});
  File.associate = function (models) {
    // associations can be defined here
  };
  return File;
};