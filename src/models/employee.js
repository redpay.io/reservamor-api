const crypto = require('crypto')

'use strict';
module.exports = (sequelize, DataTypes) => {
  const Employee = sequelize.define('Employee', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    username: {
      type: DataTypes.STRING,
      allowNull: false
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      get() {
        return () => this.getDataValue('password')
      }
    },
    displayName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    phoneNumber: {
      type: DataTypes.STRING,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: true,
      unique: true
    },
    enable: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    roleId: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: 'Role',
        key: 'id'
      }
    },
    phoneNumberVerify: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    emailVerify: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    isMaster: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    employeeStoreGroupId: {
      type: DataTypes.UUID,
      allowNull: true,
      references: {
        model: 'EmployeeStoreGroup',
        key: 'id'
      }
    },
    salt: {
      type: DataTypes.STRING,
      // allowNull: false, // for validation, but set at "beforeCreate"
      get() {
        return () => this.getDataValue('salt')
      }
    }
  }, {
    hooks: { // global hooks
      beforeValidate() {
      },
      beforeCreate() {
      },
      beforeUpdate() {
      }
    }
  });
  Employee.associate = function (models) {
    Employee.belongsTo(models.Role, {
      foreignKey: 'roleId',
      as: 'role'
    })
    Employee.belongsTo(models.EmployeeStoreGroup, {
      foreignKey: 'employeeStoreGroupId',
      as: 'group'
    })
  };

  const setSaltAndPassword = employee => {
    if (employee.changed('password')) {
      employee.salt = Employee.generateSalt()
      employee.password = Employee.encryptPassword(employee.password(), employee.salt())
    }
  }

  // **** global method ****
  Employee.generateSalt = function () {
    return crypto.randomBytes(16).toString('base64')
  }
  Employee.encryptPassword = function (plainText, salt) {
    return crypto
      .createHash('RSA-SHA256')
      .update(plainText)
      .update(salt)
      .digest('hex')
  }

  // **** global hook ****
  Employee.beforeCreate(setSaltAndPassword)
  Employee.beforeUpdate(setSaltAndPassword)

  // **** instance method ****
  Employee.prototype.correctPassword = function (enteredPassword) {
    return Employee.encryptPassword(enteredPassword, this.salt()) === this.password()
  }

  return Employee;
};
