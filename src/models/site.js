'use strict';
module.exports = (sequelize, DataTypes) => {
  const Site = sequelize.define('Site', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    storeId: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: 'Store',
        key: 'id'
      }
    },
    siteTemplateId: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: 'SiteTemplate',
        key: 'id'
      }
    },
    employeeStoreGroupId: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: 'EmployeeStoreGroup',
        key: 'id'
      }
    },
    status: {
      type: DataTypes.ENUM,
      allowNull: false,
      values: ['DRAFT', 'APPROVE', 'ACTIVE', 'INACTIVE'],
      defaultValue: 'DRAFT'
    },
  }, {});
  Site.associate = function (models) {
    Site.belongsTo(models.SiteTemplate, {
      foreignKey: 'siteTemplateId',
      as: 'template'
    })
    Site.hasOne(models.SiteContent, {
      foreignKey: 'siteId',
      as: 'content'
    })
    Site.belongsTo(models.EmployeeStoreGroup, {
      foreignKey: 'employeeStoreGroupId',
      as: 'group'
    })
    Site.belongsTo(models.Store, {
      foreignKey: 'storeId',
      as: 'store'
    })
    Site.hasMany(models.SiteGallery, {
      foreignKey: 'siteId',
      as: 'gallery'
    })
  };
  return Site;
};
