'use strict';
module.exports = (sequelize, DataTypes) => {
  const Oauth = sequelize.define('Oauth', {
    oauthid: {
      type: DataTypes.STRING,
      allowNull: false
    },
    accessToken: {
      type: DataTypes.STRING,
      allowNull: false
    },
    refreshToken: {
      type: DataTypes.STRING,
      allowNull: true
    },
    provider: {
      type: DataTypes.ENUM,
      allowNull: false,
      values: ['FACEBOOK', 'GOOGLE']
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: 'User',
        key: 'id'
      }
    },
    tokenExpired: {
      type: DataTypes.DATE,
      allowNull: true
    },
  }, {});
  Oauth.associate = function (models) {
    Oauth.belongsTo(models.User, {
      foreignKey: 'userId',
      as: 'user'
    })
  };
  return Oauth;
};
