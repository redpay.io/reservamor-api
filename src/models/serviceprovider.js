'use strict';
module.exports = (sequelize, DataTypes) => {
  const ServiceProvider = sequelize.define('ServiceProvider', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    storeId: {
      type: DataTypes.UUID,
      allowNull: true,
      references: {
        model: 'Store',
        key: 'id'
      }
    },
    avatarFileId: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: 'File',
        key: 'id'
      }
    },
    displayName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    status: {
      type: DataTypes.ENUM,
      allowNull: false,
      values: ['ACTIVE', 'INACTIVE'],
      defaultValue: 'ACTIVE'
    },
    supplyCntSameTime: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    phoneNumber: {
      type: DataTypes.STRING,
      allowNull: false
    },
    phoneNumberVerify: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: true
    },
    smsNotificationFlag: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    description: {
      type: DataTypes.STRING(1024),
      allowNull: true
    },
    employeeStoreGroupId: {
      type: DataTypes.UUID,
      allowNull: true,
      references: {
        model: 'EmployeeStoreGroup',
        key: 'id'
      }
    },
  }, {});
  ServiceProvider.associate = function (models) {
    ServiceProvider.belongsTo(models.File, {
      foreignKey: 'avatarFileId',
      as: 'avatar'
    })
    ServiceProvider.hasMany(models.Calendar, {
      foreignKey: 'serviceProviderId',
      as: 'calendars'
    })
    ServiceProvider.hasMany(models.OpeningHour, {
      foreignKey: 'serviceProviderId',
      as: 'openingHours'
    })
  };
  return ServiceProvider;
};
