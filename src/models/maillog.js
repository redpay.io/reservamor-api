'use strict';
module.exports = (sequelize, DataTypes) => {
  const MailLog = sequelize.define('MailLog', {
    status: {
      type: DataTypes.STRING,
      allowNull: false
    },
    statusMessage: {
      type: DataTypes.STRING,
      allowNull: true
    },
    host: {
      type: DataTypes.ENUM,
      allowNull: false,
      values: ['SENDGRID']
    },
    senderName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    receiver: {
      type: DataTypes.STRING,
      allowNull: false
    },
    subject: {
      type: DataTypes.STRING,
      allowNull: false
    },
    content: {
      type: DataTypes.TEXT,
      allowNull: false
    }
  }, {});
  MailLog.associate = function (models) {
    // associations can be defined here
  };
  return MailLog;
};
