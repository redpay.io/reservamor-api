'use strict';
module.exports = (sequelize, DataTypes) => {
  const SiteContent = sequelize.define('SiteContent', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    siteId: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: 'Site',
        key: 'id'
      }
    },
    bannerFileId: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: 'File',
        key: 'id'
      }
    },
    thumbnailFileId: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: 'File',
        key: 'id'
      }
    },
    description: {
      type: DataTypes.STRING(1024),
      allowNull: true
    },
  }, {});
  SiteContent.associate = function (models) {
    // associations can be defined here
  };
  return SiteContent;
};
