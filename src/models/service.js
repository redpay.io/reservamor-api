const moment = require('moment')
'use strict';
module.exports = (sequelize, DataTypes) => {
  const Service = sequelize.define('Service', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    storeId: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: 'Store',
        key: 'id'
      }
    },
    serviceCategoryId: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: 'ServiceCategory',
        key: 'id'
      }
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    durationStart: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    durationEnd: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    bookingCnt: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    priceStart: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    priceEnd: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    description: {
      type: DataTypes.STRING(1024),
      allowNull: true
    },
    imageFileId: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: 'File',
        key: 'id'
      }
    },
    orderSeq: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    status: {
      type: DataTypes.ENUM,
      allowNull: false,
      values: ['ACTIVE', 'INACTIVE', 'HIDDEN'],
      defaultValue: 'ACTIVE'
    },
    foodType: {
      type: DataTypes.STRING,
      allowNull: true
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE,
      get() {
        return moment(this.getDataValue('createdAt')).format('YYYY-MM-DD HH:mm:ss')
      }
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE,
      get() {
        return moment(this.getDataValue('updatedAt')).format('YYYY-MM-DD HH:mm:ss')
      }
    },
  }, { paranoid: true });
  Service.associate = function (models) {
    Service.belongsToMany(models.ServiceProvider, {
      through: 'ServiceAndServiceProvider',
      foreignKey: 'serviceId',
      as: 'providers'
    })
    Service.belongsTo(models.File, {
      foreignKey: 'imageFileId',
      as: 'image'
    })
    Service.belongsTo(models.ServiceCategory, {
      foreignKey: 'serviceCategoryId',
      as: 'category'
    })
    Service.belongsTo(models.Store, {
      foreignKey: 'storeId',
      as: 'store'
    })
  };
  return Service;
};
