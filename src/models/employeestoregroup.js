'use strict';
module.exports = (sequelize, DataTypes) => {
  const EmployeeStoreGroup = sequelize.define('EmployeeStoreGroup', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    masterEmployeeId: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: 'Employee',
        key: 'id'
      }
    },
    registerType: {
      type: DataTypes.ENUM,
      allowNull: false,
      values: ['PERSONAL', 'SERVICE', 'RESTAURANT']
    },
  }, {});
  EmployeeStoreGroup.associate = function (models) {
    // associations can be defined here
  };
  EmployeeStoreGroup.REGISTER_TYPE_PERSONAL = 'PERSONAL'
  EmployeeStoreGroup.REGISTER_TYPE_SERVICE = 'SERVICE'
  EmployeeStoreGroup.REGISTER_TYPE_RESTAURANT = 'RESTAURANT'
  return EmployeeStoreGroup;
};
