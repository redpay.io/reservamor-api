'use strict';
module.exports = (sequelize, DataTypes) => {
  const Role = sequelize.define('Role', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    level: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
  }, {});
  Role.associate = function (models) {
    // associations can be defined here
  };
  Role.NAME_ROOT = 'ROOT'
  Role.NAME_ADMIN = 'ADMIN'
  Role.NAME_MEMBER = 'MEMBER'
  return Role;
};
