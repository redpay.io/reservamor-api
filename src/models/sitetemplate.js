'use strict';
module.exports = (sequelize, DataTypes) => {
  const SiteTemplate = sequelize.define('SiteTemplate', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    enable: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    description: {
      type: DataTypes.STRING(1024),
      allowNull: true
    },
  }, {});
  SiteTemplate.associate = function(models) {
    // associations can be defined here
  };
  return SiteTemplate;
};
