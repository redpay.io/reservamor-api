'use strict';
module.exports = (sequelize, DataTypes) => {
  const ServiceAndServiceProvider = sequelize.define('ServiceAndServiceProvider', {
    serviceId: {
      type: DataTypes.UUID,
      // allowNull: false,
      references: {
        model: 'Service',
        key: 'id'
      }
    },
    serviceProviderId: {
      type: DataTypes.UUID,
      // allowNull: false,
      references: {
        model: 'ServiceProvider',
        key: 'id'
      }
    },
  }, {});
  ServiceAndServiceProvider.associate = function(models) {
    // associations can be defined here
  };
  return ServiceAndServiceProvider;
};
