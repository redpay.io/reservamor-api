const { Role, Employee, EmployeeStoreGroup, ServiceCategory, SiteTemplate, sequelize } = require('../models')

const seeder1 = async (transaction) => {
  const rootRole = await Role.create({ name: 'ROOT', level: 100 }, { transaction })
  const adminRole = await Role.create({ name: 'ADMIN', level: 200 }, { transaction })
  const memberRole = await Role.create({ name: 'MEMBER', level: 300 }, { transaction })

  const employee = await Employee.create({
    username: 'root',
    password: 'root88',
    displayName: 'Root',
    phoneNumber: '0911111111',
    email: null,
    roleId: rootRole.id,
    isMaster: true
  })
  const employeeStoreGroup = await EmployeeStoreGroup.create({
    masterEmployeeId: employee.id,
    registerType: EmployeeStoreGroup.REGISTER_TYPE_PERSONAL
  })
  await employee.setGroup(employeeStoreGroup)
}

const seeder2 = async (transaction) => {
  await ServiceCategory.create({ displayName: '餐廳', code: 'RESTAURANT' }, { transaction })
  await ServiceCategory.create({ displayName: '美髮', code: 'HAIR' }, { transaction })
  await ServiceCategory.create({ displayName: '美甲', code: 'NAIL' }, { transaction })
  await ServiceCategory.create({ displayName: 'spa', code: 'SPA' }, { transaction })
  await ServiceCategory.create({ displayName: '美睫', code: 'EYELASH' }, { transaction })
}

const seeder3 = async (transaction) => {
  await SiteTemplate.create({ name: '預設' }, { transaction })

}

const managedTransaction = async () => {
  try {
    await sequelize.transaction(async (transaction) => {
      await seeder1()
      await seeder2()
      await seeder3()
    })
  } catch (err) {
    console.error(err)
  }
  process.exit()
}
managedTransaction()


const unmanagedTransaction = async () => {
  console.log('unmanagedTransaction....')
  let transaction;

  try {
    transaction = await sequelize.transaction();

    await Role.create({ name: 'TEST1', level: (((1001))) }, { transaction })
    await Role.create({ name: 'TEST2', level: (((1002))) }, { transaction })
    await Role.create({ name: 'TEST3', level: (((1003))) }, { transaction })
    await transaction.commit();

  } catch (err) {
    console.log(err)
    if (transaction) await transaction.rollback();
  } finally {
    // process.exit()
  }
}
// unmanagedTransaction()
