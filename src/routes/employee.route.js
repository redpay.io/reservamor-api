const express = require('express')
const router = express.Router()
const jwt = require('../libs/jwt')
const logger = require('../libs/winston')
const employeeService = require('../services/employee.service')
import M from '../middlewares'

module.exports = (app) => {
  router.post('/register', M.validateKeySecret, async (req, res, next) => {
    try {
      logger.info('employee.route.register() invoke')
      const employee = await employeeService.registerEmployee({ payload: req.body })
      let payload = { id: employee.id }
      res.status(200).send({ token: jwt.sign(payload) })
    } catch (err) {
      next(err)
    }
  })
  router.get('/validate/duplicate', M.validateKeySecret, async (req, res, next) => {
    try {
      logger.info('employee.route.validate.duplicate() invoke')
      await employeeService.validateDuplicate(req.query)
      res.status(200).send('OK')
    } catch (err) {
      next(err)
    }
  })
  router.post('/login', M.validateBackendPassword, M.attachProfile, async (req, res, next) => {
    try {
      logger.info('employee.route.login() invoke')
      let payload = { id: req.currentEmployee.id }
      res.status(200).send({ token: jwt.sign(payload) })
    } catch (err) {
      next(err)
    }
  })
  router.get('/me', M.validateToken, M.attachProfile, M.requireIdentity(['EMPLOYEE']), async (req, res, next) => {
    try {
      const response = await employeeService.findEmployeeByPk({ employeeId: req.currentEmployee.id })
      res.status(200).send(response)
    } catch (err) {
      next(err)
    }
  })
  router.get('/', M.validateToken, M.attachProfile, M.requireIdentity(['EMPLOYEE']), async (req, res, next) => {
    try {
      logger.info('employee.route.list() invoke')
      const employees = await employeeService.listEmployees(req.query)
      res.status(200).send(employees)
    } catch (err) {
      next(err)
    }
  })
  router.post('/', M.validateToken, M.attachProfile, M.requireIdentity(['EMPLOYEE']), async (req, res, next) => {
    try {
      logger.info('employee.route.post() invoke')
      const employee = await employeeService.createEmployee({ payload: req.body })
      res.status(200).send(employee)
    } catch (err) {
      next(err)
    }
  })
  router.get('/:id', M.validateToken, M.attachProfile, M.requireIdentity(['EMPLOYEE']), async (req, res, next) => {
    try {
      logger.info('employee.route.get() invoke')
      const employee = await employeeService.findEmployeeByPk({ employeeId: req.params.id })
      res.status(200).send(employee ? employee : {})
    } catch (err) {
      next(err)
    }
  })
  router.put('/:id', M.validateToken, M.attachProfile, M.requireIdentity(['EMPLOYEE']), async (req, res, next) => {
    try {
      logger.info('employee.route.put() invoke')
      const employee = await employeeService.updateEmployee({ employeeId: req.params.id, payload: req.body })
      res.status(200).send(employee ? employee : {})
    } catch (err) {
      next(err)
    }
  })

  app.use('/employee', router)
}
