const express = require('express')
const router = express.Router()
const serviceService = require('../services/service.service')
const logger = require('../libs/winston')
import M from '../middlewares'
const multer = require('multer')
const upload = multer({ dest: process.env.UPLOAD_FILE_PATH })

module.exports = (app) => {
  router.get('/', async (req, res, next) => {
    try {
      logger.info('service.route.list() invoke')
      const services = await serviceService.listServices(req.query)
      res.status(200).send(services)
    } catch (err) {
      next(err)
    }
  })
  router.get('/category', async (req, res, next) => {
    try {
      logger.info('service.route.category.list() invoke')
      const serviceCategories = await serviceService.listServiceCategories(req.query)
      res.status(200).send(serviceCategories)
    } catch (err) {
      next(err)
    }
  })
  router.post('/', M.validateToken, M.attachProfile, M.requireIdentity(['EMPLOYEE']), upload.single('image'), async (req, res, next) => {
    try {
      logger.info('service.route.create() invoke')
      const service = await serviceService.createService({ payload: req.body, fileInfo: req.file })
      res.status(200).send(service ? service : {})
    } catch (err) {
      next(err)
    }
  })
  router.get('/:id', async (req, res, next) => {
    try {
      logger.info('service.route.get() invoke')
      const serviceDetail = await serviceService.findServiceByPk({ serviceId: req.params.id, paranoid: req.query.paranoid })
      res.status(200).send(serviceDetail ? serviceDetail : {})
    } catch (err) {
      next(err)
    }
  })
  router.put('/:id', M.validateToken, M.attachProfile, M.requireIdentity(['EMPLOYEE']), upload.single('image'), async (req, res, next) => {
    try {
      logger.info('service.route.update() invoke')
      const service = await serviceService.updateService({ serviceId: req.params.id, payload: req.body, fileInfo: req.file })
      res.status(200).send(service ? service : {})
    } catch (err) {
      next(err)
    }
  })
  router.delete('/:id', M.validateToken, M.attachProfile, M.requireIdentity(['EMPLOYEE']), async (req, res, next) => {
    try {
      logger.info('service.route.delete() invoke')
      const service = await serviceService.deleteService({ serviceId: req.params.id })
      res.status(200).send(service ? service : {})
    } catch (err) {
      next(err)
    }
  })

  app.use('/service', router)
}
