const express = require('express')
const router = express.Router()
const logger = require('../libs/winston')
const siteService = require('../services/site.service')
import M from '../middlewares'
const multer = require('multer')

const fileFilter = function (req, file, cb) {
  // supported image file mimetypes
  var allowedMimes = ['image/jpeg', 'image/pjpeg', 'image/png', 'image/gif'];

  if (allowedMimes.includes(file.mimetype)) {
    // allow supported image files
    cb(null, true);
  } else {
    // throw error for invalid files
    cb(new Error('Invalid file type. Only jpg, png and gif image files are allowed.'));
  }
};

const upload = multer({
  dest: process.env.UPLOAD_FILE_PATH, fileFilter
})
const uploadHandler = upload.fields([
  { name: 'bannerImage', maxCount: 1 },
  { name: 'thumbnailImage', maxCount: 1 },
  { name: 'gallery', maxCount: 10 }
])

module.exports = (app) => {
  router.get('/gallery/:id', async (req, res, next) => {
    try {
      logger.info('site.route.gallery.get() invoke')
      const siteGallery = await siteService.findSiteGalley({ siteId: req.params.id })
      res.status(200).send(siteGallery ? siteGallery : [])
    } catch (err) {
      next(err)
    }
  })
  router.delete('/gallery/:id', M.validateToken, M.attachProfile, M.requireIdentity(['EMPLOYEE']), async (req, res, next) => {
    try {
      logger.info('site.route.gallery.delete() invoke')
      const siteGallery = await siteService.deleteSiteGallery({ siteId: req.params.id, payload: req.body })
      res.status(200).send(siteGallery ? siteGallery : [])
    } catch (err) {
      next(err)
    }
  })
  router.get('/template', M.validateToken, M.attachProfile, M.requireIdentity(['EMPLOYEE']), async (req, res, next) => {
    try {
      logger.info('site.route.template.list() invoke')
      const siteTemplates = await siteService.listSiteTemplates(req.query)
      res.status(200).send(siteTemplates)
    } catch (err) {
      next(err)
    }
  })
  router.get('/', async (req, res, next) => {
    try {
      logger.info('site.route.list() invoke')
      const sites = await siteService.listSites(req.query)
      res.status(200).send(sites)
    } catch (err) {
      next(err)
    }
  })
  router.post('/', M.validateToken, M.attachProfile, M.requireIdentity(['EMPLOYEE']), uploadHandler, async (req, res, next) => {
    try {
      logger.info('site.route.create() invoke')
      const site = await siteService.createSite({ payload: req.body, filesInfo: req.files })
      res.status(200).send(site ? site : {})
    } catch (err) {
      next(err)
    }
  })
  router.get('/:id', async (req, res, next) => {
    try {
      logger.info('site.route.get() invoke')
      const siteDetail = await siteService.findSiteByPk({ siteId: req.params.id })
      res.status(200).send(siteDetail ? siteDetail : {})
    } catch (err) {
      next(err)
    }
  })
  router.put('/:id', M.validateToken, M.attachProfile, M.requireIdentity(['EMPLOYEE']), uploadHandler, async (req, res, next) => {
    try {
      logger.info('site.route.update() invoke')
      const store = await siteService.updateSite({ siteId: req.params.id, payload: req.body, filesInfo: req.files })
      res.status(200).send(store ? store : {})
    } catch (err) {
      next(err)
    }
  })

  app.use('/site', router)
}
