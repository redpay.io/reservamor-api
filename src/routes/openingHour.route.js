const express = require('express')
const router = express.Router()
const logger = require('../libs/winston')
const openingHourService = require('../services/openingHour.service')
import M from '../middlewares'

module.exports = (app) => {
  router.get('/', M.validateToken, M.attachProfile, async (req, res, next) => {
    try {
      logger.info('openingHour.route.list() invoke')
      const response = await openingHourService.listOpeningHours(req.query)
      res.status(200).send(response ? response : {})
    } catch (err) {
      next(err)
    }
  })
  router.delete('/:id', M.validateToken, M.attachProfile, M.requireIdentity(['EMPLOYEE']), async (req, res, next) => {
    try {
      logger.info('openingHour.route.delete() invoke')
      const openingHour = await openingHourService.deleteOpeningHour({ openingHourId: req.params.id })
      res.status(200).send(openingHour ? openingHour : {})
    } catch (err) {
      next(err)
    }
  })

  app.use('/openingHour', router)
}
