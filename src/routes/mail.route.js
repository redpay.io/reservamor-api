const express = require('express')
const router = express.Router()
const jwt = require('../libs/jwt')
const { ApiError } = require('../libs/Errors')
const logger = require('../libs/winston')
const mailService = require('../services/mail.service')
import M from '../middlewares'

module.exports = (app) => {
  router.post('/verify/:type', M.validateKeySecret, async (req, res, next) => {
    try {
      logger.info('mail.route.verify.post() invoke')
      const result = await mailService.sendVerifyCode({ type: req.params.type, email: req.query.email })
      res.status(200).send(result)
    } catch (err) {
      next(err)
    }
  })
  router.get('/verify/:type', M.validateKeySecret, async (req, res, next) => {
    try {
      logger.info('mail.route.verify.get() invoke')
      const result = await mailService.checkVerifyCode({ type: req.params.type, verifyCode: req.query.verifyCode })
      res.status(200).send(result)
    } catch (err) {
      next(err)
    }
  })

  app.use('/mail', router)
}
