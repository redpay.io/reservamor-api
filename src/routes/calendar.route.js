const express = require('express')
const router = express.Router()
const logger = require('../libs/winston')
const calendarService = require('../services/calendar.service')
import M from '../middlewares'

module.exports = (app) => {
  router.get('/', M.validateToken, M.attachProfile, async (req, res, next) => {
    try {
      logger.info('calendar.route.list() invoke')
      const response = await calendarService.listCalendars(req.query)
      res.status(200).send(response ? response : {})
    } catch (err) {
      next(err)
    }
  })
  router.delete('/:id', M.validateToken, M.attachProfile, M.requireIdentity(['EMPLOYEE']), async (req, res, next) => {
    try {
      logger.info('calendar.route.delete() invoke')
      const calendar = await calendarService.deleteCalendar({ calendarId: req.params.id })
      res.status(200).send(calendar ? calendar : {})
    } catch (err) {
      next(err)
    }
  })

  app.use('/calendar', router)
}
