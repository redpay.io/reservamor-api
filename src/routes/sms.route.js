const express = require('express')
const router = express.Router()
const logger = require('../libs/winston')
const smsService = require('../services/sms.service')
import M from '../middlewares'

module.exports = (app) => {
  router.post('/verify', M.validateKeySecret, async (req, res, next) => {
    try {
      logger.info('sms.route.verify.post() invoke')
      const result = await smsService.sendVerifyCode(req.query)
      res.status(200).send(result)
    } catch (err) {
      next(err)
    }
  })
  router.get('/verify', M.validateKeySecret, async (req, res, next) => {
    try {
      logger.info('sms.route.verify.get() invoke')
      const result = await smsService.checkVerifyCode(req.query)
      res.status(200).send(result)
    } catch (err) {
      next(err)
    }
  })


  app.use('/sms', router)
}
