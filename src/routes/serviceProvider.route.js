const express = require('express')
const router = express.Router()
const logger = require('../libs/winston')
const serviceProviderService = require('../services/serviceProvider.service')
import M from '../middlewares'
const multer = require('multer')
const upload = multer({ dest: process.env.UPLOAD_FILE_PATH })

module.exports = (app) => {
  router.get('/', async (req, res, next) => {
    try {
      logger.info('serviceProvider.route.list() invoke')
      const serviceProviders = await serviceProviderService.listServiceProviders(req.query)
      res.status(200).send(serviceProviders)
    } catch (err) {
      next(err)
    }
  })
  router.post('/', M.validateToken, M.attachProfile, M.requireIdentity(['EMPLOYEE']), upload.single('avatarImage'), async (req, res, next) => {
    try {
      logger.info('serviceProvider.route.create() invoke')
      const serviceProvider = await serviceProviderService.createServiceProvider({ payload: req.body, fileInfo: req.file })
      res.status(200).send(serviceProvider ? serviceProvider : {})
    } catch (err) {
      next(err)
    }
  })
  router.get('/:id', async (req, res, next) => {
    try {
      logger.info('serviceProvider.route.get() invoke')
      const serviceProvider = await serviceProviderService.findServiceProviderByPk({ serviceProviderId: req.params.id })
      res.status(200).send(serviceProvider ? serviceProvider : {})
    } catch (err) {
      next(err)
    }
  })
  router.put('/:id', M.validateToken, M.attachProfile, M.requireIdentity(['EMPLOYEE']), upload.single('avatarImage'), async (req, res, next) => {
    try {
      logger.info('serviceProvider.route.update() invoke')
      const serviceProvider = await serviceProviderService.updateServiceProvider({ serviceProviderId: req.params.id, payload: req.body, fileInfo: req.file })
      res.status(200).send(serviceProvider ? serviceProvider : {})
    } catch (err) {
      next(err)
    }
  })

  app.use('/serviceProvider', router)
}
