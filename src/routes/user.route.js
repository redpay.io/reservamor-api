const express = require('express')
const router = express.Router()
import M from '../middlewares'
const userService = require('../services/user.service')
const logger = require('../libs/winston')
const jwt = require('../libs/jwt')

module.exports = (app) => {
  router.post('/register', M.validateKeySecret, async (req, res, next) => {
    try {
      logger.info('user.route.register() invoke')
      const user = await userService.createUser({ payload: req.body })
      let payload = { id: user.id }
      res.status(200).send({ token: jwt.sign(payload) })
    } catch (err) {
      next(err)
    }
  })
  // router.get('/validate/duplicate', M.validateKeySecret, async (req, res, next) => {
  //   try {
  //     logger.info('user.route.validate.duplicate() invoke')
  //     await userService.validateDuplicate(req.query)
  //     res.status(200).send('OK')
  //   } catch (err) {
  //     next(err)
  //   }
  // })
  // router.post('/login', M.validateFrontendPassword, M.attachProfile, async (req, res, next) => {
  //   try {
  //     logger.info('user.route.login() invoke')
  //     let payload = { id: req.currentUser.id }
  //     res.status(200).send({ token: jwt.sign(payload) })
  //   } catch (err) {
  //     next(err)
  //   }
  // })
  // router.get('/me', M.validateToken, M.attachProfile, M.requireIdentity(['USER']), async (req, res, next) => {
  //   try {
  //     res.status(200).send(req.currentUser)
  //   } catch (err) {
  //     next(err)
  //   }
  // })
  // router.get('/', M.validateToken, M.attachProfile, M.requireIdentity(['EMPLOYEE', 'USER']), async (req, res, next) => {
  //   try {
  //     logger.info('user.route.find() invoke')
  //     const users = await userService.findUserAll(req.query)
  //     res.status(200).send(users)
  //   } catch (err) {
  //     next(err)
  //   }
  // })
  // router.post('/', M.validateToken, M.attachProfile, M.requireIdentity(['USER']), async (req, res, next) => {
  //   try {
  //     logger.info('user.route.create() invoke')
  //     const user = await userService.createUser({ payload: req.body })
  //     res.status(200).send(user)
  //   } catch (err) {
  //     next(err)
  //   }
  // })
  // router.get('/:id', M.validateToken, M.attachProfile, M.requireIdentity(['EMPLOYEE', 'USER']), async (req, res, next) => {
  //   try {
  //     logger.info('user.route.get() invoke')
  //     const user = await userService.findUserByPk({ userId: req.params.id })
  //     res.status(200).send(user ? user : {})
  //   } catch (err) {
  //     next(err)
  //   }
  // })
  // router.put('/:id', M.validateToken, M.attachProfile, M.requireIdentity(['USER']), async (req, res, next) => {
  //   try {
  //     logger.info('employee.route.put() invoke')
  //     const user = await userService.updateUser({ userId: req.params.id, payload: req.body })
  //     res.status(200).send(user ? user : {})
  //   } catch (err) {
  //     next(err)
  //   }
  // })
  app.use('/user', router)
}
