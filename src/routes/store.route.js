const express = require('express')
const router = express.Router()
const logger = require('../libs/winston')
const storeService = require('../services/store.service')
import M from '../middlewares'
const multer = require('multer')
const upload = multer({ dest: process.env.UPLOAD_FILE_PATH })

module.exports = (app) => {
  router.post('/openingHour/sync', M.validateToken, M.attachProfile, M.requireIdentity(['EMPLOYEE']), async (req, res, next) => {
    try {
      logger.info('store.route.openingHour.sync() invoke')
      const openingHours = await storeService.syncOpeningHour(req.body)
      res.status(200).send(openingHours ? openingHours : {})
    } catch (err) {
      next(err)
    }
  })
  router.get('/', async (req, res, next) => {
    try {
      logger.info('store.route.list() invoke')
      const stores = await storeService.listStores(req.query)
      res.status(200).send(stores)
    } catch (err) {
      next(err)
    }
  })
  router.post('/', M.validateToken, M.attachProfile, M.requireIdentity(['EMPLOYEE']), upload.single('logoImage'), async (req, res, next) => {
    try {
      logger.info('store.route.create() invoke')
      const store = await storeService.createStore({ payload: req.body, fileInfo: req.file })
      res.status(200).send(store ? store : {})
    } catch (err) {
      next(err)
    }
  })
  router.get('/:id', async (req, res, next) => {
    try {
      logger.info('store.route.get() invoke')
      const storeDetail = await storeService.findStoreByPk({ storeId: req.params.id })
      res.status(200).send(storeDetail ? storeDetail : {})
    } catch (err) {
      next(err)
    }
  })
  router.put('/:id', M.validateToken, M.attachProfile, M.requireIdentity(['EMPLOYEE']), upload.single('logoImage'), async (req, res, next) => {
    try {
      logger.info('store.route.update() invoke')
      const store = await storeService.updateStore({ storeId: req.params.id, payload: req.body, fileInfo: req.file })
      res.status(200).send(store ? store : {})
    } catch (err) {
      next(err)
    }
  })

  app.use('/store', router)
}
