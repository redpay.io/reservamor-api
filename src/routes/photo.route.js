const express = require('express')
const router = express.Router()
const fileService = require('../services/file.service')
const logger = require('../libs/winston')
const photoUtils = require('../libs/photoUtils')
const path = require('path')
const fs = require('fs')

const maxAge = 864000 // 10 days

module.exports = (app) => {
  router.get('/thumbnail/:id/:width?/:height?', async (req, res, next) => {
    try {
      logger.info('photo.route.thumbnail.get() invoke')
      const fileInfo = await fileService.validateFile({ fileId: req.params.id })

      if (!fs.existsSync(path.join(process.env.UPLOAD_FILE_PHOTO_THUMBNAIL_PATH, fileInfo.sysName))) {
        await photoUtils.compress(process.env.UPLOAD_FILE_PATH, fileInfo.sysName, process.env.UPLOAD_FILE_PHOTO_THUMBNAIL_PATH)
      }
      const photoBuffer = await photoUtils.resize(process.env.UPLOAD_FILE_PHOTO_THUMBNAIL_PATH, fileInfo.sysName, parseInt(req.params.width), parseInt(req.params.height))
      res.setHeader('Content-Type', fileInfo.mimeType)
      res.setHeader('Cache-Control', `public, max-age=${maxAge}`)
      res.end(photoBuffer, 'binary')
    } catch (err) {
      next(err)
    }
  })
  router.get('/:id/:width?/:height?', async (req, res, next) => {
    try {
      logger.info('photo.route.get() invoke')
      const fileInfo = await fileService.validateFile({ fileId: req.params.id })
      const photoBuffer = await photoUtils.resize(process.env.UPLOAD_FILE_PATH, fileInfo.sysName, parseInt(req.params.width), parseInt(req.params.height))
      res.setHeader('Content-Type', fileInfo.mimeType)
      res.setHeader('Cache-Control', `public, max-age=${maxAge}`)
      res.end(photoBuffer, 'binary')
    } catch (err) {
      next(err)
    }
  })
  app.use('/photo', router)
}
