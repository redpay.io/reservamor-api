const fs = require('fs')
const jwt = require('jsonwebtoken')
const path = require('path')
const appRootPath = require('app-root-path')

const privateKey = fs.readFileSync(path.join(appRootPath.toString(), 'src', 'config', 'private.key'), 'utf8')
const publicKey = fs.readFileSync(path.join(appRootPath.toString(), 'src', 'config', 'public.key'), 'utf8')

const options = {
  expiresIn: '7d', // 7 days validity
  algorithm: 'RS256'
}

module.exports = {
  sign: payload => {
    return jwt.sign(payload, privateKey, options)
  },
  verify: token => {
    try {
      return jwt.verify(token, publicKey, options)
    } catch (err) {
      return false
    }
  },
  decode: token => {
    return jwt.decode(token, { complete: true })
  }
}
