const BASE58 = '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz'
const bs58 = require('base-x')(BASE58)

/**
 * Generate random number
 * @param {Number} digits length of digits
 * @return {Number} Fixed length of digits
 */
export function generateRandomNumber(digits) {
  return Math.floor(Math.random() * 9 * Math.pow(10, digits - 1)) + Math.pow(10, digits - 1)
}

/**
 * Generate random string
 * @param {Number} length length of string
 * @return {String} Fixed length of string, include only numbers, uppercase letters and lowercase letters
 */
export function generateRandomString(length) {
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  let result = ''
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * characters.length))
  }
  return result
}

export function base58Encoding(input) {
  return bs58.encode(Buffer.from(input))
}

export function base58Decoding(encoded) {
  return bs58.decode(encoded).toString()
}

export function base64Encoding(input) {
  return Buffer.from(input).toString('base64')
}

export function base64Decoding(encoded) {
  return Buffer.from(encoded, 'base64').toString('utf-8')
}

export function convertToPascalCase(input) {
  return input.replace(/\w\S*/g, m => m.charAt(0).toUpperCase() + m.substr(1).toLowerCase())
}
