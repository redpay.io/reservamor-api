const { ApiError } = require('./Errors')
const models = require('../models')
const R = require('ramda')

function getAlias() {

}

const handler = {
  verifyModelAttribute: (attributeType, inputAttributeList, attributeList) => {
    if (inputAttributeList) {
      let diffAttributeList = R.difference(inputAttributeList)(attributeList)
      if (diffAttributeList.length > 0) {
        throw new ApiError(`Invalid attribute for ${attributeType}: ` + diffAttributeList, 400)
      }
    }
  },
  verifyModelName: (modelType, modelName) => {
    if (!models[modelName]) throw new ApiError(`Invalid model for ${modelType}: ` + modelName, 400)
  },
  verifyModelOrder: (sortbyAttributeList, orderList) => {
    const isIllegal = (n) => {
      const legalList = ['asc', 'desc']
      return !R.contains(n.toLowerCase(), legalList)
    }

    if (R.length(sortbyAttributeList) === R.length(orderList)) {
      return true
    } else if (!sortbyAttributeList && !orderList) {
      return true
    } else if (!sortbyAttributeList && orderList) {
      throw new ApiError("Unused 'order' fields", 400)
    } else if ((R.length(sortbyAttributeList) !== R.length(orderList)) && R.length(orderList) !== 1) {
      throw new ApiError("'sortby', 'order' sizes mismatch or 'order' size is not 1", 400)
    } else if (orderList && R.filter(isIllegal, orderList).length !== 0) {
      throw new ApiError("Invalid order. Must be either [asc|desc]", 400)
    }
  },
  orderConverter: (sortbyAttributeList, orderList) => {
    if (R.length(sortbyAttributeList) === R.length(orderList)) {
      let result = []
      sortbyAttributeList.forEach((value, index) => {
        result.push([value, orderList[index]])
      })
      return result
    } else if (R.length(orderList) === 1) {
      const appendOrder = x => [x, orderList[0]]
      return R.map(appendOrder, sortbyAttributeList)
    }
    return undefined
  },
  whereConverter: (queryList, queryAttributeList) => {
    let result = {}
    if (queryList) {
      queryAttributeList.forEach((value, index) => {
        result[value] = R.split(':', queryList[index])[1]
      })
    }
    return result
  },
  includeConverter: (sourceModel, nestQueryList) => {
    let includes = []

    if (nestQueryList) {
      nestQueryList.forEach((nestQuery) => {
        // 1) 確認格式, format: "ModelName.attributeName=attributeValue"
        if (!(nestQuery.indexOf('.') != -1 && nestQuery.indexOf('=') != -1)) {
          throw new ApiError(`Invalid format for nestQuery: ${nestQuery}`, 400)
        }
        let targetModel
        let targetModelName = nestQuery.split('.')[0]
        let attributeName = nestQuery.split('.')[1].split('=')[0]
        let attributeValue = nestQuery.split('.')[1].split('=')[1]
        let aliasName

        // 2) verify model
        handler.verifyModelName('nestQuery', targetModelName)
        targetModel = models[targetModelName]

        // 3) get aliasName
        Object.keys(sourceModel.associations).forEach((association) => {
          if (sourceModel.associations[association].target.name === targetModel.name) {
            aliasName = sourceModel.associations[association].as
          }
        })
        if (!aliasName) throw new ApiError(`Invalid association for nestQuery, target model: ${targetModel.name}`, 400)

        // 4) verify attributeName
        handler.verifyModelAttribute('nestQuery', [attributeName], R.keys(targetModel.rawAttributes))

        includes.push({
          model: targetModel, as: aliasName, where: {
            [attributeName]: attributeValue
          }
        })

      })
    }
    return includes
  },
  whereInConverter: (orList, orAttributeList) => {
    let result = {}
    if (orList) {
      orAttributeList.forEach((value, index) => {
        result[value] = R.split('|', R.split(':', orList[index])[1])
      })
    }
    return result
  },
  findAndCountAll: (model, { query, nestQuery, field, sortby, order, limit, offset, or }) => {
    const modelAttributeList = R.keys(model.rawAttributes)

    // step0: prepare data
    let queryList = query ? R.split(',', query) : undefined
    let queryAttributeList
    if (queryList) {
      const fetchAttribute = x => R.split(':', x)[0]
      queryAttributeList = R.map(fetchAttribute, queryList)
    }
    let orList = or ? R.split(',', or) : undefined
    let orAttributeList
    if (orList) {
      const fetchAttribute = x => R.split(':', x)[0]
      orAttributeList = R.map(fetchAttribute, orList)
    }
    let nestQueryList = nestQuery ? R.split(',', nestQuery) : undefined

    let fieldAttributeList = field ? R.split(',', field) : undefined
    let sortbyAttributeList = sortby ? R.split(',', sortby) : undefined
    let orderList = order ? R.split(',', order) : undefined
    limit = limit ? parseInt(limit) : 10
    offset = offset ? parseInt(offset) : undefined

    // step1: check illegal attribute
    handler.verifyModelAttribute('query', queryAttributeList, modelAttributeList)
    handler.verifyModelAttribute('or', orAttributeList, modelAttributeList)
    handler.verifyModelAttribute('field', fieldAttributeList, modelAttributeList)
    handler.verifyModelAttribute('sortby', sortbyAttributeList, modelAttributeList)
    handler.verifyModelOrder(sortbyAttributeList, orderList)

    let options = {
      // raw: true // turns off metadata
    }
    // step2-1: handle "query" & "or"
    options.where = {
      ...handler.whereConverter(queryList, queryAttributeList),
      ...handler.whereInConverter(orList, orAttributeList)
    }

    // step2-2: handle "field"
    options.attributes = fieldAttributeList

    // step2-3: handle "sortby"
    options.order = handler.orderConverter(sortbyAttributeList, orderList)

    // step2-4: handle "limit"
    if (limit > 0) {
      options.limit = limit
    }

    // step2-5: handle "offset"
    options.offset = offset

    // step2-6: handle "include"
    options.include = handler.includeConverter(model, nestQueryList)

    // step3: model querying
    return model.findAndCountAll(options)
  }
}

module.exports = handler
