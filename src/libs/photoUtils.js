const path = require('path')
const sharp = require('sharp')
const fs = require('fs')
const imagemin = require('imagemin')
const imageminPngquant = require('imagemin-pngquant')

/**
 * Get image file info
 * @param {string} filePath
 * @param {string} fileName
 * @returns File info include fomrat, size, width, height
 */
export async function getProfile(filePath, fileName) {
  try {
    const fileBuffer = fs.readFileSync(path.join(filePath, fileName));
    const image = sharp(fileBuffer)
    const { format, size, width, height } = await image.metadata()
    return { format, size, width, height }
  } catch (err) {
    throw err
  }
}

/**
 *
 * @param {string} filePath
 * @param {string} fileName
 * @param {integer} width
 * @param {integer} height
 * @returns A buffer object
 */
export async function resize(filePath, fileName, width, height) {
  try {
    let options = {}
    if (width) options.width = width
    if (height) options.height = height
    return await sharp(path.join(filePath, fileName)).resize(options).toBuffer()
  } catch (err) {
    throw err
  }
}

/**
 * Compress image file size to 10%~30% of origin file size
 * @param {string} srcFilePath
 * @param {string} srcFileName
 * @param {string} targetFilePath
 */
export async function compress(srcFilePath, srcFileName, targetFilePath) {
  try {
    const { format } = await getProfile(srcFilePath, srcFileName)
    fs.mkdirSync(targetFilePath, { recursive: true });
    if (format === 'jpeg') {
      await sharp(path.join(srcFilePath, srcFileName))
        .withMetadata()
        .jpeg({ quality: 30 })
        .toFile(path.join(targetFilePath, srcFileName));
    } else if (format === 'png') {
      await imagemin([path.join(srcFilePath, srcFileName)], {
        destination: targetFilePath,
        plugins: [
          imageminPngquant({
            quality: [0.3, 0.4]
          })
        ]
      })
    } else {
      throw new Error('Invalid format. Only jpeg and png are supported.')
    }
  } catch (err) {
    throw err
  }
}
