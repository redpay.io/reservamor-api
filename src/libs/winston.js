const appRootPath = require('app-root-path')
const { createLogger, format, transports } = require('winston')
require('winston-daily-rotate-file')

let logDir = `${appRootPath}/logs/${process.env.NODE_ENV}`

let options = {
  level: 'info',
  format: format.combine(
    format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss.SSS'
    }),
    format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
  ),
  transports: [
    new transports.Console(),
    new transports.DailyRotateFile({
      dirname: logDir,
      filename: `%DATE%.log`,
      datePattern: 'YYYY-MM-DD',
      zippedArchive: true
    })
  ]
}

const logger = createLogger(options)

module.exports = logger
