import axios from 'axios'

const key = process.env.GOOGLE_MAP_API_KEY

export async function geocode({ address, language = 'zh-TW' }) {
  let latitude
  let longitude
  try {
    const url = process.env.GOOGLE_AP_API_SERVER + '/geocode/json'
    const response = await axios.get(url, { params: { key, language, address } })
    const results = response.data.results
    const status = response.data.status

    if (status.toUpperCase() === 'OK' && results.length) {
      latitude = results[0].geometry.location.lat
      longitude = results[0].geometry.location.lng
    } else {
      throw new Error(`Invalid address: ${address}`)
    }

    return { latitude, longitude }
  } catch (err) {
    throw err
  }
}

export async function getPlaceId({ latitude, longitude, name, radius = 100, language = 'zh-TW' }) {
  let placeId
  try {
    const url = process.env.GOOGLE_AP_API_SERVER + '/place/nearbysearch/json'
    const response = await axios.get(url, { params: { key, language, location: `${latitude},${longitude}`, radius, name } })
    const results = response.data.results
    const status = response.data.status

    if (status.toUpperCase() === 'OK' && results.length) {
      placeId = results[0].place_id
    } else {
      throw new Error(`No place was found: latitude=${latitude}, longitude=${longitude}, name=${name}, radius=${radius}`)
    }

    return placeId
  } catch (err) {
    throw err
  }
}

export async function getPlaceDetails({ placeId, language = 'zh-TW' }) {
  let openingHours
  let openingHourPeriods
  let rating
  try {
    const url = process.env.GOOGLE_AP_API_SERVER + '/place/details/json'
    const response = await axios.get(url, { params: { key, language, place_id: placeId } })
    const result = response.data.result
    const status = response.data.status

    if (status.toUpperCase() === 'OK') {
      if (result.opening_hours && result.opening_hours.weekday_text && result.opening_hours.weekday_text.length) {
        openingHours = result.opening_hours.weekday_text.join('\n')
      }
      if (result.opening_hours && result.opening_hours.periods && result.opening_hours.periods.length) {
        openingHourPeriods = result.opening_hours.periods
      }
      rating = result.rating
    } else {
      throw new Error(`Invalid placeId: ${placeId}`)
    }

    return { openingHours, rating, openingHourPeriods }
  } catch (err) {
    throw err
  }
}

export async function getLocationInfo({ address, name, language = 'zh-TW' }) {
  let latitude
  let longitude
  let placeId
  let openingHours
  let openingHourPeriods
  let rating

  try {
    const coordinate = await geocode({ address, language })
    latitude = coordinate.latitude
    longitude = coordinate.longitude
    placeId = await getPlaceId({ latitude, longitude, name })
    const details = await getPlaceDetails({ placeId })
    openingHours = details.openingHours
    rating = details.rating
    openingHourPeriods = details.openingHourPeriods

  } catch (err) {
    console.error(err)
  } finally {
    return { latitude, longitude, placeId, openingHours, openingHourPeriods, rating }
  }
}
