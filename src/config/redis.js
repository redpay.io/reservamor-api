import redis from 'async-redis'

const options = {
  host: process.env.REDIS_SERVER_URL,
  port: process.env.REDIS_SERVER_PORT,
  password: process.env.REDIS_PASSWORD
}

const client = redis.createClient(options)
client.on('error', function (err) {
  console.log('Error ' + err);
});

// export default client
module.exports = client
