if (!process.env.PROJECT_NAME) {
  require('dotenv-flow').config();
}

module.exports = {
  "development": {
    "username": process.env.DB_USERNAME,
    "password": process.env.DB_PASSWORD,
    "database": process.env.DB_NAME,
    "host": process.env.DB_HOSTNAME,
    "dialect": "mysql",
    "dialectOptions": {
      "dateStrings": true,
      "typeCast": true
    },
    "timezone": "+08:00",
    "define": {
      "charset": "utf8mb4",
      "collate": "utf8mb4_general_ci",
      "underscored": false,
      "freezeTableName": true,
      "timestamps": true
    }
  },
  "test": {
    "username": process.env.DB_USERNAME,
    "password": process.env.DB_PASSWORD,
    "database": process.env.DB_NAME,
    "host": process.env.DB_HOSTNAME,
    "dialect": "mysql",
    "dialectOptions": {
      "dateStrings": true,
      "typeCast": true
    },
    "timezone": "+08:00",
    "define": {
      "charset": "utf8mb4",
      "collate": "utf8mb4_general_ci",
      "underscored": false,
      "freezeTableName": true,
      "timestamps": true
    }
  },
  "production": {
    "username": process.env.DB_USERNAME,
    "password": process.env.DB_PASSWORD,
    "database": process.env.DB_NAME,
    "host": process.env.DB_HOSTNAME,
    "dialect": "mysql",
    "dialectOptions": {
      "dateStrings": true,
      "typeCast": true
    },
    "timezone": "+08:00",
    "define": {
      "charset": "utf8mb4",
      "collate": "utf8mb4_general_ci",
      "underscored": false,
      "freezeTableName": true,
      "timestamps": true
    }
  }
}
