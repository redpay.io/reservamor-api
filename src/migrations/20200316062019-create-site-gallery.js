'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('SiteGallery', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4
      },
      siteId: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: 'Site',
          key: 'id'
        }
      },
      fileId: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: 'File',
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('SiteGallery');
  }
};
