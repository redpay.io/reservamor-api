'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    try {
      await queryInterface.sequelize.transaction({}, async (transaction) => {
        await queryInterface.addColumn(
          'OpeningHour',
          'serviceProviderId',
          {
            type: Sequelize.UUID,
            allowNull: true,
            defaultValue: Sequelize.UUIDV4
          },
          { transaction }
        )

        await queryInterface.addConstraint('OpeningHour', ['serviceProviderId'], {
          type: 'foreign key',
          references: { //Required field
            table: 'ServiceProvider',
            field: 'id'
          }
        });
      })
    } catch (err) {
      throw err
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.removeColumn('OpeningHour', 'serviceProviderId', { transaction });
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  }
};

