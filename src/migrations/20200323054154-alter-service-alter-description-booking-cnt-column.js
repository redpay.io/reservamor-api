'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    try {
      await queryInterface.sequelize.transaction({}, async (transaction) => {
        await queryInterface.changeColumn('Service', 'bookingCnt', {
          type: Sequelize.INTEGER,
          allowNull: true
        })
        await queryInterface.changeColumn('Service', 'description', {
          type: Sequelize.STRING(1024),
          allowNull: true
        })
      })
    } catch (err) {
      throw err
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.sequelize.transaction({}, async (transaction) => {
        await queryInterface.changeColumn('Service', 'bookingCnt', {
          type: Sequelize.INTEGER,
          allowNull: false
        })
        await queryInterface.changeColumn('Service', 'description', {
          type: Sequelize.STRING(1024),
          allowNull: false
        })
      })
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  }
};
