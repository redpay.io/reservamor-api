'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Site', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4
      },
      storeId: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: 'Store',
          key: 'id'
        }
      },
      siteTemplateId: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: 'SiteTemplate',
          key: 'id'
        }
      },
      employeeStoreGroupId: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: 'EmployeeStoreGroup',
          key: 'id'
        }
      },
      status: {
        type: Sequelize.ENUM,
        allowNull: false,
        values: ['DRAFT', 'APPROVE', 'ACTIVE', 'INACTIVE'],
        defaultValue: 'DRAFT'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Site');
  }
};
