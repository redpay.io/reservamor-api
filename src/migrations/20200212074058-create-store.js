'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Store', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4
      },
      no: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      phoneNumber: {
        type: Sequelize.STRING,
        allowNull: false
      },
      city1: {
        type: Sequelize.STRING,
        allowNull: false
      },
      city2: {
        type: Sequelize.STRING,
        allowNull: false
      },
      address: {
        type: Sequelize.STRING,
        allowNull: false
      },
      status: {
        type: Sequelize.ENUM,
        allowNull: false,
        values: ['DRAFT', 'READY', 'ACTIVE'],
        defaultValue: 'DRAFT'
      },
      logoFileId: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: 'File',
          key: 'id'
        }
      },
      communityUrl: {
        type: Sequelize.STRING,
        allowNull: true
      },
      isBranch: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },
      latitude: {
        type: Sequelize.DOUBLE,
        allowNull: true
      },
      longitude: {
        type: Sequelize.DOUBLE,
        allowNull: true
      },
      placeId: {
        type: Sequelize.STRING,
        allowNull: true
      },
      rating: {
        type: Sequelize.FLOAT,
        allowNull: true
      },
      employeeStoreGroupId: {
        type: Sequelize.UUID,
        allowNull: true,
        references: {
          model: 'EmployeeStoreGroup',
          key: 'id'
        }
      },
      description: {
        type: Sequelize.STRING(1024),
        allowNull: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Store');
  }
};
