'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('OpeningHour', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4
      },
      storeId: {
        type: Sequelize.UUID,
        allowNull: true,
        references: {
          model: 'Store',
          key: 'id'
        }
      },
      weekDay: {
        type: Sequelize.ENUM,
        allowNull: false,
        values: ['0', '1', '2', '3', '4', '5', '6']
      },
      isBusinessDay: {
        type: Sequelize.BOOLEAN,
        allowNull: false
      },
      periodName: {
        type: Sequelize.STRING,
        allowNull: true
      },
      openTime: {
        type: Sequelize.STRING,
        allowNull: true
      },
      closeTime: {
        type: Sequelize.STRING,
        allowNull: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('OpeningHour');
  }
};
