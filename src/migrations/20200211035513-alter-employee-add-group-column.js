'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    try {
      await queryInterface.sequelize.transaction({}, async (transaction) => {
        await queryInterface.addColumn(
          'Employee',
          'employeeStoreGroupId',
          {
            type: Sequelize.UUID,
            allowNull: true,
            defaultValue: Sequelize.UUIDV4
          },
          { transaction }
        )

        await queryInterface.addConstraint('Employee', ['employeeStoreGroupId'], {
          type: 'foreign key',
          references: { //Required field
            table: 'EmployeeStoreGroup',
            field: 'id'
          }
        });
      })
    } catch (err) {
      throw err
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.removeColumn('Employee', 'employeeStoreGroupId', { transaction });
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  }
};
