'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    try {
      await queryInterface.sequelize.transaction({}, async (transaction) => {
        await queryInterface.removeColumn('Service', 'duration', { transaction });
        await queryInterface.addColumn(
          'Service',
          'durationStart',
          {
            type: Sequelize.INTEGER,
            allowNull: false
          },
          { transaction }
        )
        await queryInterface.addColumn(
          'Service',
          'durationEnd',
          {
            type: Sequelize.INTEGER,
            allowNull: false
          },
          { transaction }
        )
      })
    } catch (err) {
      throw err
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.removeColumn('Service', 'durationStart', { transaction });
      await queryInterface.removeColumn('Service', 'durationEnd', { transaction });
      await queryInterface.addColumn(
        'Service',
        'duration',
        {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        { transaction }
      )
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  }
};
