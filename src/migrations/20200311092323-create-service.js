'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Service', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4
      },
      storeId: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: 'Store',
          key: 'id'
        }
      },
      serviceCategoryId: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: 'ServiceCategory',
          key: 'id'
        }
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      duration: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      bookingCnt: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      priceStart: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      priceEnd: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      description: {
        type: Sequelize.STRING(1024),
        allowNull: false
      },
      imageFileId: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: 'File',
          key: 'id'
        }
      },
      orderSeq: {
        type: Sequelize.INTEGER,
        allowNull: true
      },
      status: {
        type: Sequelize.ENUM,
        allowNull: false,
        values: ['ACTIVE', 'INACTIVE', 'HIDDEN'],
        defaultValue: 'ACTIVE'
      },
      foodType: {
        type: Sequelize.STRING,
        allowNull: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Service');
  }
};
