'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('EmployeeStoreGroup', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4
      },
      masterEmployeeId: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: 'Employee',
          key: 'id'
        }
      },
      registerType: {
        type: Sequelize.ENUM,
        allowNull: false,
        values: ['PERSONAL', 'SERVICE', 'RESTAURANT']
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('EmployeeStoreGroup');
  }
};
