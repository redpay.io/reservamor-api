'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    try {
      await queryInterface.sequelize.transaction({}, async (transaction) => {
        await queryInterface.changeColumn('Service', 'durationStart', {
          type: Sequelize.INTEGER,
          allowNull: true
        })
        await queryInterface.changeColumn('Service', 'durationEnd', {
          type: Sequelize.INTEGER,
          allowNull: true
        })
        await queryInterface.changeColumn('Service', 'priceStart', {
          type: Sequelize.INTEGER,
          allowNull: true
        })
        await queryInterface.changeColumn('Service', 'priceEnd', {
          type: Sequelize.INTEGER,
          allowNull: true
        })
      })
    } catch (err) {
      throw err
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.changeColumn('Service', 'durationStart', {
        type: Sequelize.INTEGER,
        allowNull: false
      })
      await queryInterface.changeColumn('Service', 'durationEnd', {
        type: Sequelize.INTEGER,
        allowNull: false
      })
      await queryInterface.changeColumn('Service', 'priceStart', {
        type: Sequelize.INTEGER,
        allowNull: false
      })
      await queryInterface.changeColumn('Service', 'priceEnd', {
        type: Sequelize.INTEGER,
        allowNull: false
      })
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  }
};
