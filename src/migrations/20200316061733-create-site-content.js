'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('SiteContent', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4
      },
      siteId: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: 'Site',
          key: 'id'
        }
      },
      bannerFileId: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: 'File',
          key: 'id'
        }
      },
      thumbnailFileId: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: 'File',
          key: 'id'
        }
      },
      description: {
        type: Sequelize.STRING(1024),
        allowNull: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('SiteContent');
  }
};
