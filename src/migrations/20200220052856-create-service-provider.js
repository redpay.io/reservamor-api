'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ServiceProvider', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4
      },
      storeId: {
        type: Sequelize.UUID,
        allowNull: true,
        references: {
          model: 'Store',
          key: 'id'
        }
      },
      avatarFileId: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: 'File',
          key: 'id'
        }
      },
      displayName: {
        type: Sequelize.STRING,
        allowNull: false
      },
      status: {
        type: Sequelize.ENUM,
        allowNull: false,
        values: ['ACTIVE', 'INACTIVE'],
        defaultValue: 'ACTIVE'
      },
      supplyCntSameTime: {
        type: Sequelize.INTEGER,
        allowNull: true
      },
      phoneNumber: {
        type: Sequelize.STRING,
        allowNull: false
      },
      phoneNumberVerify: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },
      email: {
        type: Sequelize.STRING,
        allowNull: true
      },
      smsNotificationFlag: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },
      description: {
        type: Sequelize.STRING(1024),
        allowNull: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('ServiceProvider');
  }
};
