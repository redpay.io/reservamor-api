'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    try {
      await queryInterface.sequelize.transaction({}, async (transaction) => {
        await queryInterface.addColumn(
          'Calendar',
          'serviceProviderId',
          {
            type: Sequelize.UUID,
            allowNull: true,
            defaultValue: Sequelize.UUIDV4
          },
          { transaction }
        )

        await queryInterface.addConstraint('Calendar', ['serviceProviderId'], {
          type: 'foreign key',
          references: { //Required field
            table: 'ServiceProvider',
            field: 'id'
          }
        });
      })
    } catch (err) {
      throw err
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.removeColumn('Calendar', 'serviceProviderId', { transaction });
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  }
};
