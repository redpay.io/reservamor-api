const { Role, sequelize } = require('../models')
const modelHandler = require('../libs/modelHandler')
const logger = require('../libs/winston')
const { ApiError } = require('../libs/Errors')

const service = {
  async validateRole({ roleId }) {
    logger.info('role.service.validateRole() invoke')
    const role = await Role.findByPk(roleId)
    if (!role) throw new ApiError(`Invalid roleId: ${roleId}`, 400)
    return role
  },
  async validateRoleByName({ roleName }) {
    logger.info('role.service.validateRoleByName() invoke')
    const role = await Role.findOne({ where: { name: roleName } })
    if (!role) throw new ApiError(`Invalid roleName: ${roleName}`, 400)
    return role
  },
  listRoles({ query, field, sortby, order, limit, offset }) {
    logger.info('role.service.listRoles() invoke')
    return modelHandler.findAndCountAll(Role, { query, field, sortby, order, limit, offset })
  },
  findRoleByPk({ roleId }) {
    logger.info('role.service.findRoleByPk() invoke')
    return Role.findByPk(roleId)
  }
}
module.exports = service
