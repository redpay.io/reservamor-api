const { Calendar, sequelize } = require('../models')
const modelHandler = require('../libs/modelHandler')
const logger = require('../libs/winston')
const R = require('ramda')
const validateService = require('./validate.service')
const calendarAttributeList = R.keys(Calendar.rawAttributes)
const moment = require('moment')
const { ApiError } = require('../libs/Errors')

const service = {
  // 執行時，外層需要有transaction，當有exception時，才會rollback
  async createCalendars({ storeId, serviceProviderId, items }) {
    logger.info('calendar.service.createCalendars() invoke')
    try {
      if (storeId) {
        await validateService.checkDataPk({ id: storeId, modelName: 'Store' })
      } else if (serviceProviderId) {
        await validateService.checkDataPk({ id: serviceProviderId, modelName: 'ServiceProvider' })
      } else {
        throw new ApiError('storeId or serviceProviderId is required', 400)
      }

      for (let i = 0; i < items.length; i++) {
        let payload = items[i]
        if (storeId) payload.storeId = storeId
        if (serviceProviderId) payload.serviceProviderId = serviceProviderId

        // 1) prepare data
        payload['year'] = moment(payload.closeDate, 'YYYY-MM-DD').format('YYYY');
        payload['month'] = moment(payload.closeDate, 'YYYY-MM-DD').format('M');

        // 2) validate input
        modelHandler.verifyModelAttribute('body', R.keys(payload), calendarAttributeList)
        await Calendar.create(payload)
      }
    } catch (err) {
      throw err
    }
  },
  async listCalendars({ storeId, serviceProviderId, year, month }) {
    logger.info('calendar.service.listCalendars() invoke')
    let instance
    let where = {}
    if (storeId) {
      instance = await validateService.checkDataPk({ id: storeId, modelName: 'Store' })
    } else if (serviceProviderId) {
      instance = await validateService.checkDataPk({ id: serviceProviderId, modelName: 'ServiceProvider' })
    } else {
      throw new ApiError('storeId or serviceProviderId is required', 400)
    }

    if (!year) throw new ApiError('year is required', 400)

    where.year = year
    if (month) where.month = month
    return instance.getCalendars({
      where, order: [
        ['closeDate', 'ASC']
      ],
    })
  },
  // 執行時，外層需要有transaction，當有exception時，才會rollback
  async updateCalendars({ storeId, serviceProviderId, items }) {
    logger.info('calendar.service.updateCalendars() invoke')
    try {
      if (storeId) {
        await validateService.checkDataPk({ id: storeId, modelName: 'Store' })
      } else if (serviceProviderId) {
        await validateService.checkDataPk({ id: serviceProviderId, modelName: 'ServiceProvider' })
      } else {
        throw new ApiError('storeId or serviceProviderId is required', 400)
      }

      for (let i = 0; i < items.length; i++) {
        let payload = items[i]
        if (storeId) payload.storeId = storeId
        if (serviceProviderId) payload.serviceProviderId = serviceProviderId

        // 1) prepare data
        payload['year'] = moment(payload.closeDate, 'YYYY-MM-DD').format('YYYY');
        payload['month'] = moment(payload.closeDate, 'YYYY-MM-DD').format('M');

        // 2) validate input
        modelHandler.verifyModelAttribute('body', R.keys(payload), calendarAttributeList)

        if (payload.id) {
          // 3) update
          let calendar = await validateService.checkDataPk({ id: payload.id, modelName: 'Calendar' })
          await calendar.update(payload)
        } else {
          // 4) insert
          await Calendar.create(payload)
        }
      }
    } catch (err) {
      throw err
    }
  },
  async deleteCalendar({ calendarId }) {
    logger.info('calendar.service.deleteCalendar() invoke')

    // step1: validate FK
    const calendar = await validateService.checkDataPk({ id: calendarId, modelName: 'Calendar' })

    // step2: delete
    return calendar.destroy()
  },
}

module.exports = service
