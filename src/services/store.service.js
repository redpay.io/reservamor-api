const { Store, File, sequelize } = require('../models')
const modelHandler = require('../libs/modelHandler')
const logger = require('../libs/winston')
const mapUtils = require('../libs/mapUtils')
const R = require('ramda')
const storeAttributeList = R.keys(Store.rawAttributes)
const validateService = require('./validate.service')
const openingHourService = require('./openingHour.service')
const calendarService = require('./calendar.service')
const fileService = require('./file.service')

const service = {
  listStores({ query, field, sortby, order, limit, offset, or }) {
    logger.info('store.service.listStores() invoke')
    return modelHandler.findAndCountAll(Store, { query, field, sortby, order, limit, offset, or })
  },
  async createStore({ payload, fileInfo }) {
    logger.info('store.service.createStore() invoke')
    let storePayload = payload.store ? JSON.parse(payload.store) : {}
    let openingHoursPayload = payload.openingHours ? JSON.parse(`[${payload.openingHours}]`) : []
    let calendarsPayload = payload.calendars ? JSON.parse(`[${payload.calendars}]`) : []
    let storeResponse

    // 1) validate input
    modelHandler.verifyModelAttribute('body', R.keys(storePayload), storeAttributeList)

    // 2) check FK
    await validateService.checkDataPk({ id: storePayload.employeeStoreGroupId, modelName: 'EmployeeStoreGroup' })

    try {
      await sequelize.transaction({}, async (transaction) => {
        if (fileInfo) {
          // 3) insert into file
          const file = await fileService.createFile({ fileInfo })
          storePayload.logoFileId = file.id
        }

        // 4) get latitude & longitude
        if (storePayload.address) {
          let fullAddress = storePayload.city1 + storePayload.city2 + storePayload.address
          const { latitude, longitude, placeId, rating } = await mapUtils.getLocationInfo({ address: fullAddress, name: storePayload.name })
          if (latitude) storePayload['latitude'] = latitude
          if (longitude) storePayload['longitude'] = longitude
          if (placeId) storePayload['placeId'] = placeId
          if (rating) storePayload['rating'] = rating
        }

        // 5) insert into store
        storePayload.no = Store.genNo()
        storeResponse = await Store.create(storePayload)

        // 6) openingHour
        if (openingHoursPayload.length) {
          await openingHourService.createOpeningHours({
            storeId: storeResponse.id,
            items: openingHoursPayload
          })
        }

        // 7) calendar
        if (calendarsPayload.length) {
          await calendarService.createCalendars({
            storeId: storeResponse.id,
            items: calendarsPayload
          })
        }
      })

      return storeResponse
    } catch (err) {
      throw err
    }
  },
  async findStoreByPk({ storeId }) {
    logger.info('store.service.findStoreByPk() invoke')
    let storeResponse
    let logoResponse
    let groupResponse
    storeResponse = await Store.findByPk(storeId)
    if (storeResponse) {
      groupResponse = await storeResponse.getGroup()
      logoResponse = await storeResponse.getLogo()
    }

    return {
      store: storeResponse,
      group: groupResponse,
      logo: logoResponse
    }
  },
  async updateStore({ payload, storeId, fileInfo }) {
    logger.info('store.service.updateStore() invoke')
    let storePayload = payload.store ? JSON.parse(payload.store) : {}
    let openingHoursPayload = payload.openingHours ? JSON.parse(`[${payload.openingHours}]`) : []
    let calendarsPayload = payload.calendars ? JSON.parse(`[${payload.calendars}]`) : []
    let storeResponse

    // 1) validate input
    modelHandler.verifyModelAttribute('body', R.keys(storePayload), storeAttributeList)

    // 2) check FK
    let store = await validateService.checkDataPk({ id: storeId, modelName: 'Store' })
    if (storePayload.employeeStoreGroupId) await validateService.checkDataPk({ id: storePayload.employeeStoreGroupId, modelName: 'EmployeeStoreGroup' })

    try {
      await sequelize.transaction({}, async (transaction) => {
        if (fileInfo) {
          // 3) insert into file
          const file = await fileService.createFile({ fileInfo })
          storePayload.logoFileId = file.id
        }

        // 4) get latitude & longitude
        if (storePayload.address) {
          let fullAddress = storePayload.city1 + storePayload.city2 + storePayload.address
          const { latitude, longitude, placeId, rating } = await mapUtils.getLocationInfo({ address: fullAddress, name: storePayload.name })
          if (latitude) storePayload['latitude'] = latitude
          if (longitude) storePayload['longitude'] = longitude
          if (placeId) storePayload['placeId'] = placeId
          if (rating) storePayload['rating'] = rating
        }

        // 5) update store
        storeResponse = await store.update(storePayload)

        // 6) openingHour
        if (openingHoursPayload.length) {
          await openingHourService.updateOpeningHours({
            storeId: storeResponse.id,
            items: openingHoursPayload
          })
        }

        // 7) calendar
        if (calendarsPayload.length) {
          await calendarService.updateCalendars({
            storeId: storeResponse.id,
            items: calendarsPayload
          })
        }
      })
      return storeResponse
    } catch (err) {
      throw err
    }
  },
  async syncOpeningHour({ name, address }) {
    const { openingHourPeriods } = await mapUtils.getLocationInfo({ address, name })
    let result = []
    if (openingHourPeriods) {
      result = openingHourPeriods.map((period) => {
        return {
          weekDay: period.close.day.toString(),
          isBusinessDay: true,
          openTime: period.open.time,
          closeTime: period.close.time
        }
      })
    }
    return result
  }
}

module.exports = service
