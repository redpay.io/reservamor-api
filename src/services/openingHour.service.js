const { OpeningHour, sequelize } = require('../models')
const modelHandler = require('../libs/modelHandler')
const logger = require('../libs/winston')
const R = require('ramda')
const validateService = require('./validate.service')
const openingHourAttributeList = R.keys(OpeningHour.rawAttributes)
const { ApiError } = require('../libs/Errors')

const service = {
  // 執行時，外層需要有transaction，當有exception時，才會rollback
  async createOpeningHours({ storeId, serviceProviderId, items }) {
    logger.info('openingHour.service.createOpeningHours() invoke')
    try {
      if (storeId) {
        await validateService.checkDataPk({ id: storeId, modelName: 'Store' })
      } else if (serviceProviderId) {
        await validateService.checkDataPk({ id: serviceProviderId, modelName: 'ServiceProvider' })
      } else {
        throw new ApiError('storeId or serviceProviderId is required', 400)
      }

      for (let i = 0; i < items.length; i++) {
        let payload = items[i]
        if (storeId) payload.storeId = storeId
        if (serviceProviderId) payload.serviceProviderId = serviceProviderId

        // 1) validate input
        modelHandler.verifyModelAttribute('body', R.keys(payload), openingHourAttributeList)
        await OpeningHour.create(payload)
      }
    } catch (err) {
      throw err
    }
  },
  // 執行時，外層需要有transaction
  async updateOpeningHours({ storeId, serviceProviderId, items }) {
    logger.info('openingHour.service.updateOpeningHours() invoke')
    try {
      if (storeId) {
        await validateService.checkDataPk({ id: storeId, modelName: 'Store' })
      } else if (serviceProviderId) {
        await validateService.checkDataPk({ id: serviceProviderId, modelName: 'ServiceProvider' })
      } else {
        throw new ApiError('storeId or serviceProviderId is required', 400)
      }

      for (let i = 0; i < items.length; i++) {
        let payload = items[i]
        // 1) validate input
        modelHandler.verifyModelAttribute('body', R.keys(payload), openingHourAttributeList)

        if (payload.id) {
          // 2) update
          let openingHour = await validateService.checkDataPk({ id: payload.id, modelName: 'OpeningHour' })
          await openingHour.update(payload)
        } else {
          if (storeId) payload.storeId = storeId
          if (serviceProviderId) payload.serviceProviderId = serviceProviderId

          // 3) insert
          modelHandler.verifyModelAttribute('body', R.keys(payload), openingHourAttributeList)
          await OpeningHour.create(payload)
        }
      }
    } catch (err) {
      throw err
    }
  },
  async deleteOpeningHour({ openingHourId }) {
    logger.info('openingHour.service.deleteOpeningHour() invoke')

    // step1: validate FK
    const openingHour = await validateService.checkDataPk({ id: openingHourId, modelName: 'OpeningHour' })

    // step2: delete
    return openingHour.destroy()
  },
  async listOpeningHours({ storeId, serviceProviderId }) {
    logger.info('openingHour.service.listOpeningHours() invoke')
    let instance
    let where = {}
    if (storeId) {
      instance = await validateService.checkDataPk({ id: storeId, modelName: 'Store' })
    } else if (serviceProviderId) {
      instance = await validateService.checkDataPk({ id: serviceProviderId, modelName: 'ServiceProvider' })
    } else {
      throw new ApiError('storeId or serviceProviderId is required', 400)
    }

    // order by weekDay, openTime
    return instance.getOpeningHours({
      where, order: [
        ['weekDay', 'ASC'],
        ['openTime', 'ASC'],
      ],
    })
  }
}

module.exports = service
