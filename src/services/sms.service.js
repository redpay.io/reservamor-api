const axios = require('axios')
const qs = require('qs')
const logger = require('../libs/winston')
const redis = require('../config/redis')
const { ApiError } = require('../libs/Errors')
const utility = require('../libs/utility')

function validateResponse(responseStr) {
  // CREDIT,SENDED,COST,UNSEND,BATCH_ID

  let responseArray = responseStr.split(',')
  // 1) check "credit" >= 0
  if (parseInt(responseArray[0]) < 0) {
    throw new ApiError(`SMS was sent failed: ${responseArray[1]}`, 500)
  } else if (parseInt(responseArray[2]) < 1) {
    throw new ApiError(`SMS was unsend`, 500)
  }
}

const service = {
  async send({ destination, message }) {
    const response = await axios({
      method: 'post',
      url: `${process.env.EVERY8D_API_SERVER}${process.env.EVERY8D_API_PATH}`,
      data: qs.stringify({
        'UID': process.env.EVERY8D_UID,
        'PWD': process.env.EVERY8D_PWD,
        'DEST': destination,
        'MSG': message,
        // 'ST': '20201231123456'
      }),
      headers: {
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
      }
    })
    logger.info('sms.send.response: ' + response.data)
    validateResponse(response.data)
  },
  async sendVerifyCode({ destination }) {
    logger.info('sms.service.sendVerifyCode() invoke')
    // 1) generate random string
    const verifyCode = utility.generateRandomNumber(6)

    // 2) insert redis
    const redisResponse = await redis.setex(destination, 300, verifyCode)

    if (redisResponse.toUpperCase() === 'OK') {
      // 3) send sms
      let env = process.env.NODE_ENV === 'production' ? '' : `[${process.env.NODE_ENV}]`
      await this.send({ destination, message: env + `您的 ${process.env.PRODUCT_NAME} 驗證碼：${verifyCode}` })
      return 'OK'
    } else {
      throw new ApiError(`Could not set verify code into Redis`, 500)
    }
  },
  async checkVerifyCode({ destination, verifyCode }) {
    logger.info('sms.service.sendVerifyCode() invoke')
    const value = await redis.get(destination)
    if (value === verifyCode) {
      return 'OK'
    } else {
      throw new ApiError(`Verify code was checked failed `, 500)
    }
  }
}

module.exports = service
