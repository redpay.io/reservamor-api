const { Service, ServiceCategory, sequelize } = require('../models')
const modelHandler = require('../libs/modelHandler')
const logger = require('../libs/winston')
const R = require('ramda')
const serviceAttributeList = R.keys(Service.rawAttributes)
const validateService = require('./validate.service')
const fileService = require('./file.service')

const service = {
  listServices({ query, field, sortby, order, limit, offset, or }) {
    logger.info('service.service.listServices() invoke')
    return modelHandler.findAndCountAll(Service, { query, field, sortby, order, limit, offset, or })
  },
  listServiceCategories({ query, field, sortby, order, limit, offset, or }) {
    logger.info('service.service.listServiceCategories() invoke')
    return modelHandler.findAndCountAll(ServiceCategory, { query, field, sortby, order, limit, offset, or })
  },
  async createService({ payload, fileInfo }) {
    logger.info('service.service.createStore() invoke')
    let servicePayload = payload.service ? JSON.parse(payload.service) : {}
    let serviceProvidersPayload = payload.serviceProviders ? payload.serviceProviders.split(',') : []
    let serviceResponse

    try {
      await sequelize.transaction({}, async (transaction) => {
        // 1) validate input
        modelHandler.verifyModelAttribute('body', R.keys(servicePayload), serviceAttributeList)

        // 2) check FK
        let serviceProviders = []
        await validateService.checkDataPk({ id: servicePayload.storeId, modelName: 'Store' })
        await validateService.checkDataPk({ id: servicePayload.serviceCategoryId, modelName: 'ServiceCategory' })
        for (let i = 0; i < serviceProvidersPayload.length; i++) {
          serviceProviders.push(await validateService.checkDataPk({ id: serviceProvidersPayload[i], modelName: 'ServiceProvider' }))
        }

        if (fileInfo) {
          // 3) insert into File
          const file = await fileService.createFile({ fileInfo })
          servicePayload.imageFileId = file.id
        }

        // 4) insert into Service
        serviceResponse = await Service.create(servicePayload)

        // 5) insert into ServiceAndServiceProviders
        await serviceResponse.addProviders(serviceProviders)
      })
      return serviceResponse
    } catch (err) {
      throw err
    }
  },
  async findServiceByPk({ serviceId, paranoid = true }) {
    logger.info('service.service.findStoreByPk() invoke')
    let serviceResponse
    let imageResponse
    let categoryResponse
    let providersResponse

    serviceResponse = await validateService.checkDataPk({ id: serviceId, modelName: 'Service', paranoid })
    imageResponse = await serviceResponse.getImage()
    categoryResponse = await serviceResponse.getCategory()
    providersResponse = await serviceResponse.getProviders()

    return {
      service: serviceResponse,
      category: categoryResponse,
      image: imageResponse,
      providers: providersResponse,
      store: await serviceResponse.getStore()
    }
  },
  async updateService({ payload, serviceId, fileInfo }) {
    logger.info('service.service.updateService() invoke')
    let servicePayload = payload.service ? JSON.parse(payload.service) : {}
    let serviceProvidersPayload = payload.serviceProviders ? payload.serviceProviders.split(',') : []
    let serviceResponse

    try {
      // 1) validate input
      await sequelize.transaction({}, async (transaction) => {
        modelHandler.verifyModelAttribute('body', R.keys(servicePayload), serviceAttributeList)

        // 2) check FK
        let serviceProviders = []
        let service = await validateService.checkDataPk({ id: serviceId, modelName: 'Service' })
        if (servicePayload.storeId) await validateService.checkDataPk({ id: servicePayload.storeId, modelName: 'Store' })
        if (servicePayload.serviceCategoryId) await validateService.checkDataPk({ id: servicePayload.serviceCategoryId, modelName: 'ServiceCategory' })
        for (let i = 0; i < serviceProvidersPayload.length; i++) {
          serviceProviders.push(await validateService.checkDataPk({ id: serviceProvidersPayload[i], modelName: 'ServiceProvider' }))
        }

        if (fileInfo) {
          // 3) insert into File
          const file = await fileService.createFile({ fileInfo })
          servicePayload.imageFileId = file.id
        }

        // 4) update Service
        serviceResponse = await service.update(servicePayload)
        serviceResponse.changed('updatedAt', true)
        serviceResponse = await service.save()

        // 5) update ServiceAndServiceProviders
        if (serviceProviders.length) await serviceResponse.setProviders(serviceProviders)
      })
      return serviceResponse
    } catch (err) {
      throw err
    }
  },
  async deleteService({ serviceId }) {
    logger.info('service.service.deleteService() invoke')

    // 1) check PK
    let service = await validateService.checkDataPk({ id: serviceId, modelName: 'Service' })

    // 2) delete
    return service.destroy()
  },
}

module.exports = service
