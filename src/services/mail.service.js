const { MailLog, User, Employee, sequelize } = require('../models')
const axios = require('axios')
const logger = require('../libs/winston')
const utility = require('../libs/utility')
const redis = require('../config/redis')
const crypto = require('crypto')
const { ApiError } = require('../libs/Errors')

const VERIFY_TYPE_EMPLOYEE = 'employee'
const VERIFY_TYPE_USER = 'user'
const TYPE_LIST = [VERIFY_TYPE_EMPLOYEE, VERIFY_TYPE_USER];

function sendBySendGrid({ subject, receiver, htmlContent }) {
  const url = process.env.SENDGRID_API_SERVER + process.env.SENDGRID_API_PATH
  const apiKey = process.env.SENDGRID_API_KEY
  const headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + apiKey
  }

  let env = process.env.NODE_ENV === 'production' ? '' : `[${process.env.NODE_ENV}]`
  let senderName = `${env} ${process.env.PRODUCT_NAME}`
  let senderAddress = 'rredpay.io@gmail.com'
  let textContent = 'Your email client does not support HTML'

  const body = {
    'personalizations': [{
      'to': [{ 'email': receiver }]
    }],
    'from': { 'email': senderAddress, 'name': senderName },
    'subject': subject,
    'content': [
      { 'type': 'text/plain', 'value': textContent },
      { 'type': 'text/html', 'value': htmlContent }
    ]
  }
  let payload = {
    host: 'SENDGRID',
    senderName,
    receiver,
    subject,
    content: htmlContent
  }
  axios.post(url, body, { headers })
    .then(function (response) {
      logger.info('mail was sent succeffully by sendgrid')
      payload.status = response.status
      payload.status = response.statusText
    })
    .catch(function (error) {
      payload.status = error.response.status
      payload.statusMessage = error.response.statusText
      logger.error(error)
    })
    .finally(function () {
      MailLog.create(payload)
    })
}

const service = {
  template: {
    orderCreate() { },
    orderActive() { },
    messageSent() { },
    verify(verifyCode, type) {
      let host = type === 'employee' ? process.env.BACKEND_SERVER_URL: process.env.FRONTEND_SERVER_URL
      let url = `${process.env.BACKEND_SERVER_URL}/mail/verify?verifyCode=${verifyCode}`
      return ''
        .concat('For your account security, please click the link below to confirm your email address')
        .concat(`<br><a href="${url}">${url}</a>`)
        .concat('<br>')
        .concat('<br>This link will expire in 4 hours')
        .concat('<br>---')
        .concat(`<br> send from ${process.env.PRODUCT_NAME}`)
    }
  },
  sendOrderCreate() {
    const content = this.template.orderCreate()
    sendBySendGrid({ content })
  },
  async sendVerifyCode({ email, type }) {
    logger.info('mail.service.sendVerifyCode() invoke')
    // 1) validate input
    if (!TYPE_LIST.includes(type)) throw new ApiError(`Invalid type: ${type}`, 400)

    // 2) generate random string
    const randomHash = crypto.createHash('md5').update(utility.generateRandomString(10)).digest("hex").toLowerCase()

    // 3) insert redis
    const redisKey = `mail:verify:${type}:${email}`
    const redisResponse = await redis.setex(redisKey, 60 * 60 * 4, randomHash)

    // 4) base58(email)
    const verifyCode = utility.base58Encoding(email) + '-' + randomHash

    if (redisResponse.toUpperCase() === 'OK') {
      // 5) send mail
      sendBySendGrid({ subject: 'Please confirm your email address', receiver: email, htmlContent: service.template.verify(verifyCode, type) })
      return 'OK'
    } else {
      throw new ApiError(`Could not set verify code into Redis`, 500)
    }
  },
  async checkVerifyCode({ verifyCode, type }) {
    logger.info('mail.service.checkUserVerifyCode() invoke')
    // 1) validate input
    if (!TYPE_LIST.includes(type)) throw new ApiError(`Invalid type: ${type}`, 400)
    let verifyCodeArray = verifyCode.split('-')
    if (verifyCodeArray.length !== 2) throw new ApiError(`Verify code was invalid`, 500)

    // 2) base58Decode
    let email = utility.base58Decoding(verifyCodeArray[0])

    // 3) validate email
    let instance
    if (type === VERIFY_TYPE_EMPLOYEE) {
      instance = await Employee.findOne({ where: { email, enable: true } })
    } else if (type === VERIFY_TYPE_USER) {
      instance = await User.findOne({ where: { email, enable: true } })
    }
    if (!instance) throw new ApiError(`Email was invalid`, 500)

    // 4) validate code
    const redisKey = `mail:verify:${type}:${email}`
    const value = await redis.get(redisKey)
    if (value === verifyCodeArray[1]) {
      await redis.del(redisKey)
    } else {
      throw new ApiError(`Verify code was checked failed`, 500)
    }

    // 5) update user set emailVerify = true
    try {
      await instance.update({ emailVerify: true })
    } catch (err) {
      throw new ApiError(`${type} was updated failed`, 500)
    }
    return 'OK'
  },
}

module.exports = service
