const { File, sequelize } = require('../models')
const logger = require('../libs/winston')
const validateService = require('./validate.service')

const service = {
  async validateFile({ fileId }) {
    logger.info('file.service.validateFile() invoke')
    const file = await validateService.checkDataPk({ id: fileId, modelName: 'File' })
    return file
  },
  createFile({ fileInfo }) {
    return File.create({
      originName: fileInfo.originalname,
      sysName: fileInfo.filename,
      mimeType: fileInfo.mimetype,
      size: fileInfo.size
    })
  }
}

module.exports = service
