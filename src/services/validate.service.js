const models = require('../models')
const logger = require('../libs/winston')
const { ApiError } = require('../libs/Errors')

const service = {
  async checkDataPk({ id, modelName, paranoid = true }) {
    logger.info('validate.service.checkDataPk() invoke')
    const modelInstance = await models[modelName].findByPk(id, { paranoid })
    if (!modelInstance) throw new ApiError(`Invalid ${modelName}.id: ${id}`, 400)
    return modelInstance
  }
}
module.exports = service
