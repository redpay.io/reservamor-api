const { Employee, EmployeeStoreGroup, Role, sequelize } = require('../models')
const logger = require('../libs/winston')
const R = require('ramda')
const { ApiError } = require('../libs/Errors')
const modelHandler = require('../libs/modelHandler')
const roleService = require('./role.service')
const validateService = require('./validate.service')
const employeeAttributeList = R.keys(Employee.rawAttributes)

const service = {
  listEmployees({ query, field, sortby, order, limit, offset, or }) {
    logger.info('employee.service.listEmployees() invoke')
    return modelHandler.findAndCountAll(Employee, { query, field, sortby, order, limit, offset, or })
  },
  async registerEmployee({ payload }) {
    logger.info('employee.service.registerEmployee() invoke')
    // 1) validate input
    if (!payload.registerType) throw new ApiError(`registerType is required: ${payload.registerType}`, 400)

    let employeeResponse
    await sequelize.transaction({}, async (transaction) => {
      // 2-1) set employee.roleId
      const role = await roleService.validateRoleByName({ roleName: Role.NAME_MEMBER })
      payload.employee.roleId = role.id

      // 2-2) validate input
      modelHandler.verifyModelAttribute('body', R.keys(payload.employee), employeeAttributeList)

      // 2-3) create employee
      employeeResponse = await Employee.create(payload.employee)

      // 3) create group
      const employeeStoreGroup = await EmployeeStoreGroup.create({
        masterEmployeeId: employeeResponse.id,
        registerType: payload.registerType
      })
      employeeResponse = await employeeResponse.setGroup(employeeStoreGroup)
    })
    return employeeResponse
  },
  async createEmployee({ payload }) {
    logger.info('employee.service.createEmployee() invoke')
    // 1) check FK
    await validateService.checkDataPk({ id: payload.employeeStoreGroupId, modelName: 'EmployeeStoreGroup' })

    let employeeResponse
    await sequelize.transaction({}, async (transaction) => {
      // 2-1) prepare data
      const role = await roleService.validateRoleByName({ roleName: Role.NAME_MEMBER })
      payload.roleId = role.id
      payload.isMaster = false

      // 2-2) validate input
      modelHandler.verifyModelAttribute('body', R.keys(payload), employeeAttributeList)

      // 2-3) create employee
      employeeResponse = await Employee.create(payload)
    })
    return employeeResponse
  },
  async validateDuplicate({ username, email }) {
    logger.info('employee.service.validateDuplicate() invoke')
    if (username && (await Employee.findOne({ where: { username } }))) {
      throw new ApiError(`Duplicate username: ${username}`, 400)
    } else if (email && await Employee.findOne({ where: { email } })) {
      throw new ApiError(`Duplicate email: ${email}`, 400)
    }
  },
  async findEmployeeByPk({ employeeId }) {
    logger.info('employee.service.findEmployeeByPk() invoke')
    const employeeResponse = await validateService.checkDataPk({ id: employeeId, modelName: 'Employee' })
    return {
      employee: employeeResponse,
      group: await employeeResponse.getGroup()
    }
  },
  async updateEmployee({ payload, employeeId }) {
    logger.info('employee.service.updateEmployee() invoke')

    // step0: clean data
    delete payload.id

    // step1: check FK
    const employee = await validateService.checkDataPk({ id: employeeId, modelName: 'Employee' })
    if (payload.employeeStoreGroupId) await validateService.checkDataPk({ id: payload.employeeStoreGroupId, modelName: 'EmployeeStoreGroup' })
    if (payload.roleId) await validateService.checkDataPk({ id: payload.roleId, modelName: 'Role' })

    // step2: validate input
    modelHandler.verifyModelAttribute('body', R.keys(payload), employeeAttributeList)

    return employee.update(payload)
  },
  authenticateOld({ username, password }) {
    logger.info('employee.service.authenticate() invoke')
    return Employee.findOne({
      where: { username, password, enable: true }
    })
  },
  async authenticate({ username, password }) {
    logger.info('employee.service.authenticate() invoke')
    const employee = await Employee.findOne({
      where: { username, enable: true }
    })
    if (employee.correctPassword(password)) return employee
  },
}

module.exports = service
