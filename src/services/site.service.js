const { Site, SiteContent, SiteGallery, SiteTemplate, File, sequelize } = require('../models')
const modelHandler = require('../libs/modelHandler')
const logger = require('../libs/winston')
const R = require('ramda')
const siteAttributeList = R.keys(Site.rawAttributes)
const siteContentAttributeList = R.keys(SiteContent.rawAttributes)
const validateService = require('./validate.service')
const fileService = require('./file.service')
const { ApiError } = require('../libs/Errors')

const service = {
  listSites({ query, nestQuery, field, sortby, order, limit, offset, or }) {
    logger.info('site.service.listSites() invoke')
    return modelHandler.findAndCountAll(Site, { query, nestQuery, field, sortby, order, limit, offset, or })
  },
  listSiteTemplates({ query, field, sortby, order, limit, offset, or }) {
    logger.info('site.service.listSiteTemplates() invoke')
    return modelHandler.findAndCountAll(SiteTemplate, { query, field, sortby, order, limit, offset, or })
  },
  async createSite({ payload, filesInfo }) {
    logger.info('site.service.createSite() invoke')
    let sitePayload = payload.site ? JSON.parse(payload.site) : {}
    let contentPayload = payload.content ? JSON.parse(payload.content) : {}
    let siteResponse

    // 1) validate input
    modelHandler.verifyModelAttribute('body', R.keys(sitePayload), siteAttributeList)
    modelHandler.verifyModelAttribute('body', R.keys(contentPayload), siteContentAttributeList)

    // 2) check FK
    await validateService.checkDataPk({ id: sitePayload.storeId, modelName: 'Store' })
    await validateService.checkDataPk({ id: sitePayload.siteTemplateId, modelName: 'SiteTemplate' })
    await validateService.checkDataPk({ id: sitePayload.employeeStoreGroupId, modelName: 'EmployeeStoreGroup' })

    try {
      await sequelize.transaction({}, async (transaction) => {
        // 3) insert into Site
        siteResponse = await Site.create(sitePayload)

        if (filesInfo.bannerImage && filesInfo.bannerImage.length) {
          // 4-1) save "banner"
          const file = await fileService.createFile({ fileInfo: filesInfo.bannerImage[0] })
          contentPayload.bannerFileId = file.id
        }
        if (filesInfo.thumbnailImage && filesInfo.thumbnailImage.length) {
          // 4-2) save "thumbnail"
          const file = await fileService.createFile({ fileInfo: filesInfo.thumbnailImage[0] })
          contentPayload.thumbnailFileId = file.id
        }
        // 4-3) insert into SiteContent
        contentPayload.siteId = siteResponse.id
        await SiteContent.create(contentPayload)

        if (filesInfo.gallery) {
          // 5) save "gallery"
          for (let i = 0; i < filesInfo.gallery.length; i++) {
            const file = await fileService.createFile({ fileInfo: filesInfo.gallery[i] })
            let galleryPayload = {
              siteId: siteResponse.id,
              fileId: file.id
            }
            await SiteGallery.create(galleryPayload)
          }
        }
      })

      return siteResponse
    } catch (err) {
      throw err
    }
  },
  async findSiteByPk({ siteId }) {
    logger.info('site.service.findSiteByPk() invoke')
    let siteResponse
    let groupResponse
    let contentResponse
    let templateResponse
    let storeResponse

    siteResponse = await Site.findByPk(siteId)
    if (siteResponse) {
      groupResponse = await siteResponse.getGroup()
      templateResponse = await siteResponse.getTemplate()
      storeResponse = await siteResponse.getStore()
      contentResponse = await siteResponse.getContent()
    }

    return {
      site: siteResponse,
      group: groupResponse,
      content: contentResponse,
      store: storeResponse,
      template: templateResponse
    }
  },
  async findSiteGalley({ siteId }) {
    logger.info('site.service.findSiteGalley() invoke')
    const site = await validateService.checkDataPk({ id: siteId, modelName: 'Site' })
    return site.getGallery()
  },
  async deleteSiteGallery({ siteId, payload }) {
    logger.info('site.service.deleteSiteGallery() invoke')

    // 1) find & check
    const siteGallery = await SiteGallery.findOne({
      where: {
        siteId: siteId,
        fileId: payload.fileId
      }
    })
    if (!siteGallery) throw new ApiError(`Invalid siteId(${siteId} or invalid fileId(${payload.fileId}))`, 400)

    // 2) delete
    return siteGallery.destroy()
  },
  async updateSite({ siteId, payload, filesInfo }) {
    logger.info('site.service.updateSite() invoke')
    let sitePayload = payload.site ? JSON.parse(payload.site) : {}
    let contentPayload = payload.content ? JSON.parse(payload.content) : {}
    let siteResponse

    // 1) validate input
    modelHandler.verifyModelAttribute('body', R.keys(sitePayload), siteAttributeList)
    modelHandler.verifyModelAttribute('body', R.keys(contentPayload), siteContentAttributeList)

    // 2) check FK
    let site = await validateService.checkDataPk({ id: siteId, modelName: 'Site' })
    if (sitePayload.storeId) await validateService.checkDataPk({ id: sitePayload.storeId, modelName: 'Store' })
    if (sitePayload.siteTemplateId) await validateService.checkDataPk({ id: sitePayload.siteTemplateId, modelName: 'SiteTemplate' })
    if (sitePayload.employeeStoreGroupId) await validateService.checkDataPk({ id: sitePayload.employeeStoreGroupId, modelName: 'EmployeeStoreGroup' })

    try {
      await sequelize.transaction({}, async (transaction) => {
        // 3) update Site
        siteResponse = await site.update(sitePayload)

        if (filesInfo && filesInfo.bannerImage && filesInfo.bannerImage.length) {
          // 4-1) save "banner"
          const file = await fileService.createFile({ fileInfo: filesInfo.bannerImage[0] })
          contentPayload.bannerFileId = file.id
        }
        if (filesInfo && filesInfo.thumbnailImage && filesInfo.thumbnailImage.length) {
          // 4-2) save "thumbnail"
          const file = await fileService.createFile({ fileInfo: filesInfo.thumbnailImage[0] })
          contentPayload.thumbnailFileId = file.id
        }
        // 4-3) update SiteContent
        await (await siteResponse.getContent()).update(contentPayload)

        if (filesInfo && filesInfo.gallery) {
          // 5) save "gallery"
          for (let i = 0; i < filesInfo.gallery.length; i++) {
            const file = await fileService.createFile({ fileInfo: filesInfo.gallery[i] })
            let galleryPayload = {
              siteId: siteResponse.id,
              fileId: file.id
            }
            await SiteGallery.create(galleryPayload)
          }
        }
      })

      return siteResponse
    } catch (err) {
      throw err
    }
  },
}

module.exports = service
