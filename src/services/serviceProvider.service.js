const { ServiceProvider, sequelize } = require('../models')
const modelHandler = require('../libs/modelHandler')
const logger = require('../libs/winston')
const R = require('ramda')
const serviceProviderAttributeList = R.keys(ServiceProvider.rawAttributes)
const validateService = require('./validate.service')
const openingHourService = require('./openingHour.service')
const calendarService = require('./calendar.service')
const fileService = require('./file.service')

const service = {
  listServiceProviders({ query, field, sortby, order, limit, offset, or }) {
    logger.info('serviceProvider.service.listServiceProviders() invoke')
    return modelHandler.findAndCountAll(ServiceProvider, { query, field, sortby, order, limit, offset, or })
  },
  async createServiceProvider({ payload, fileInfo }) {
    logger.info('serviceProvider.service.createServiceProvider() invoke')
    let serviceProviderPayload = payload.serviceProvider ? JSON.parse(payload.serviceProvider) : {}
    let openingHoursPayload = payload.openingHours ? JSON.parse(`[${payload.openingHours}]`) : []
    let calendarsPayload = payload.calendars ? JSON.parse(`[${payload.calendars}]`) : []
    let serviceProviderResponse

    // 1) validate input
    modelHandler.verifyModelAttribute('body', R.keys(serviceProviderPayload), serviceProviderAttributeList)

    // 2) check FK
    await validateService.checkDataPk({ id: serviceProviderPayload.storeId, modelName: 'Store' })
    await validateService.checkDataPk({ id: serviceProviderPayload.employeeStoreGroupId, modelName: 'EmployeeStoreGroup' })

    try {
      await sequelize.transaction({}, async (transaction) => {
        if (fileInfo) {
          // 3) insert into file
          const file = await fileService.createFile({ fileInfo })
          serviceProviderPayload.avatarFileId = file.id
        }

        // 4) insert into serviceProvider
        serviceProviderResponse = await ServiceProvider.create(serviceProviderPayload)

        // 5) openingHour
        if (openingHoursPayload.length) {
          await openingHourService.createOpeningHours({
            serviceProviderId: serviceProviderResponse.id,
            items: openingHoursPayload
          })
        }

        // 6) calendar
        if (calendarsPayload.length) {
          await calendarService.createCalendars({
            serviceProviderId: serviceProviderResponse.id,
            items: calendarsPayload
          })
        }
      })

      return serviceProviderResponse
    } catch (err) {
      throw err
    }
  },
  async findServiceProviderByPk({ serviceProviderId }) {
    logger.info('serviceProvider.service.findServiceProviderByPk() invoke')
    let serviceProviderResponse
    let avatarResponse

    serviceProviderResponse = await ServiceProvider.findByPk(serviceProviderId)
    if (serviceProviderResponse) {
      avatarResponse = await serviceProviderResponse.getAvatar()
    }

    return {
      serviceProvider: serviceProviderResponse,
      avatar: avatarResponse
    }
  },
  async updateServiceProvider({ payload, serviceProviderId, fileInfo }) {
    logger.info('serviceProvider.service.updateServiceProvider() invoke')
    let serviceProviderPayload = payload.serviceProvider ? JSON.parse(payload.serviceProvider) : {}
    let openingHoursPayload = payload.openingHours ? JSON.parse(`[${payload.openingHours}]`) : []
    let calendarsPayload = payload.calendars ? JSON.parse(`[${payload.calendars}]`) : []
    let serviceProviderResponse

    // 1) validate input
    modelHandler.verifyModelAttribute('body', R.keys(serviceProviderPayload), serviceProviderAttributeList)

    // 2) check FK
    const serviceProvider = await validateService.checkDataPk({ id: serviceProviderId, modelName: 'ServiceProvider' })
    if (serviceProviderPayload.storeId) await validateService.checkDataPk({ id: serviceProviderPayload.storeId, modelName: 'Store' })
    if (serviceProviderPayload.employeeStoreGroupId) await validateService.checkDataPk({ id: serviceProviderPayload.employeeStoreGroupId, modelName: 'EmployeeStoreGroup' })

    try {
      await sequelize.transaction({}, async (transaction) => {
        if (fileInfo) {
          // 3) insert into file
          const file = await fileService.createFile({ fileInfo })
          serviceProviderPayload.avatarFileId = file.id
        }

        // 4) update serviceProvider
        serviceProviderResponse = await serviceProvider.update(serviceProviderPayload)

        // 5) openingHour
        if (openingHoursPayload.length) {
          await openingHourService.updateOpeningHours({
            serviceProviderId: serviceProviderResponse.id,
            items: openingHoursPayload
          })
        }

        // 6) calendar
        if (calendarsPayload.length) {
          await calendarService.updateCalendars({
            serviceProviderId: serviceProviderResponse.id,
            items: calendarsPayload
          })
        }
      })
      return serviceProviderResponse
    } catch (err) {
      throw err
    }
  },
}

module.exports = service
