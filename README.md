## .env.development.local
```
DB_USERNAME=root
DB_PASSWORD=53434976
DB_NAME=reservamor_development
DB_HOSTNAME=127.0.0.1
BACKEND_SERVER_URL=http://localhost:8081
FRONTEND_SERVER_URL=http://localhost:8080
```
## sequelize cli
```
NODE_ENV=development npx sequelize db:create --charset utf8mb4 --collate utf8mb4_general_ci
NODE_ENV=development npx sequelize db:migrate
NODE_ENV=development node src/seeders/0.init.js
```
