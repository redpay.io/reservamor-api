## step2: generate migration
npx sequelize model:generate --name Role --attributes name:string,level:integer
npx sequelize model:generate --name File --attributes originName:string,sysName:string,mimeType:string,size:bigint,imageWidth:integer,imageHeight:integer
npx sequelize model:generate --name Employee --attributes username:string,password:string,displayName:string,phoneNumber:string,email:string,enable:boolean,roleId:string,phoneNumberVerify:boolean,emailVerify:boolean,isMaster:boolean
npx sequelize model:generate --name EmployeeStoreGroup --attributes masterEmployeeId:string,registerType:integer
npx sequelize migration:create --name alter-employee-add-group-column

npx sequelize model:generate --name Store --attributes no:string,name:string,phoneNumber:string,city1:string,city2:string,address:string,status:string,logoFileId:string,communityUrl:string,isBranch:boolean,latitude:double,longitude:double,placeId:string,rating:float,employeeStoreGroupId:string

npx sequelize model:generate --name OpeningHour --attributes storeId:string,weekDay:integer,isDayOff:boolean,periodName:string,openTime:string,closeTime:string
npx sequelize model:generate --name Calendar --attributes storeId:string,year:integer,month:integer,closeDate:date

npx sequelize model:generate --name ServiceProvider --attributes storeId:string,avatarFileId:string,displayName:string,status:string,supplyCntSameTime:integer,phoneNumber:string,phoneNumberVerify:boolean,email:string,smsNotificationFlag:boolean,description:string

NODE_ENV=development npx sequelize model:generate --name MailLog --attributes status:string,statusMessage:string,host:string,senderName:string,recevier:string,subject:string,content:text

npx sequelize migration:create --name alter-calendar-add-service-provider
npx sequelize migration:create --name alter-opening-hour-add-service-provider-id-column

npx sequelize migration:create --name alter-employee-add-salt-column
npx sequelize migration:create --name alter-service-provider-add-group-id-column

npx sequelize model:generate --name Service --attributes storeId:string,serviceCategoryId:string,name:string,duration:integer,bookingCnt:integer,priceStart:integer,priceEnd:integer,description:string,imageFileId:string,orderSeq:integer,status:string,foodType:string
npx sequelize model:generate --name ServiceCategory --attributes name:string,enable:boolean
npx sequelize model:generate --name ServiceAndServiceProvider --attributes serviceId:string,serviceProviderId:string

npx sequelize model:generate --name SiteTemplate --attributes name:string,enable:boolean,description:string
npx sequelize model:generate --name Site --attributes storeId:string,siteTemplateId:string,employeeStoreGroupId:string,status:string
npx sequelize model:generate --name SiteContent --attributes siteId:string,bannerFileId:string,thumbnailFileId:string,description:string
npx sequelize model:generate --name SiteGallery --attributes siteId:string,fileId:string


npx sequelize migration:create --name alter-service-alter-duration-column
npx sequelize migration:create --name alter-service-alter-duration-price-column
npx sequelize migration:create --name alter-service-alter-description-booking-cnt-column

npx sequelize model:generate --name User --attributes username:string,password:string,displayName:string,location:string,email:string,phoneNumber:string,enable:boolean,gender:boolean,birthday:date,oauth:boolean,emailVerify:boolean,phoneNumberVerify:boolean
npx sequelize model:generate --name Oauth --attributes oauthid:string,accessToken:string,refreshToken:string,provider:string,userId:string,tokenExpired:date
